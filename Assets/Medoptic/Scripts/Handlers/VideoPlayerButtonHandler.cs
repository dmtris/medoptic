﻿using UnityEngine;
using UnityEngine.Video;
using RenderHeads.Media.AVProVideo;

// Requires SpawnAtRuntime, VideoPlayerHandler, & DeviceManager in order to work so this just makes sure its on the same Object if added.
[RequireComponent(
    typeof(SpawnAtRuntime),
    typeof(VideoPlayerHandler),
    typeof(DeviceManager)
    )]
public class VideoPlayerButtonHandler : MonoBehaviour
{
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ VideoPlayerButtonHandler +++ ---- ++++
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++

    /* 
     * NOTES: 
     * Need to instantiate the buttons and the button menu rather then having them initially be part of the controller prefab.
     * This will make it easier to change the video scripts and prefabs imo down the road.
     * 
     */

    // -------------------------------------------------------------
    // VideoPlayerButtonHandler Variables
    // -------------------------------------------------------------

    // Reference to video playing in sphere 
    private MediaPlayer avProMediaPlayer;

    // Reference to the SpawnAtRuntime script
    private SpawnAtRuntime spawnAtRuntime;

    // Reference to the DeviceManager script
    private DeviceManager deviceManager;

    // Used when checking if video has been paused after initializing
    private bool videosPaused = false;

    // Remote Menu Prefab
    public GameObject buttonPlayerMenuPrefab;

    // Global Variant
    private GameObject buttonPlayerMenu;

    // Floating Video Players
    private VideoPlayer
        buttonVideoPlayer1,
        buttonVideoPlayer2,
        buttonVideoPlayer3,
        buttonVideoPlayer4;


    // Reference to avProVideo MediaPlayer
    private float
        currentAVProTime,
        mediaPlayerTime = 0.0f;

    // -------------------------------------------------------------
    // VideoPlayerButtonHandler Core Functions
    // -------------------------------------------------------------

    private void Awake()
    {
        // Get the SpawnAtRuntime script from the current GameObject
        spawnAtRuntime = gameObject.GetComponent<SpawnAtRuntime>();

        // Get the DeviceManager script from the current GameObject
        deviceManager = gameObject.GetComponent<DeviceManager>();

        // Get the AVProMediaPlayer
        avProMediaPlayer = spawnAtRuntime.GetMediaPlayer();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Set currentAVProTime to the current AVProVideo MediaPlayer Time
        currentAVProTime = avProMediaPlayer.Control.GetCurrentTimeMs();
    }

    // -------------------------------------------------------------
    // VideoPlayerButtonHandler Get / Set Functions
    // -------------------------------------------------------------

    // Get the current AVProMediaplayer time
    public float GetCurrentAVProTime() { return currentAVProTime; }

    // Get the Controller Video Menu
    public GameObject GetVideoPlayerButtonMenuPrefab() { return buttonPlayerMenuPrefab; }

    // Get the Controller Video Menu
    public GameObject GetVideoPlayerButtonMenuObj() { return buttonPlayerMenu; }

    // Passing the floating gameobject - will be changed later for dynamic use
    public void SetVideoPlayerButtonMenuObj(GameObject which) { buttonPlayerMenu = which; }

    // Passing the floating gameobject - will be changed later for dynamic use
    public GameObject GetVideoPlayerButtonObject(int button)
    {
        switch (button)
        {
            case 1:
                return deviceManager.GetControllerGameObject().transform.Find("FloatingRemoteUI").GetChild(0).GetChild(0).gameObject;
            case 2:
                return deviceManager.GetControllerGameObject().transform.Find("FloatingRemoteUI").GetChild(0).GetChild(1).gameObject;
            case 3:
                return deviceManager.GetControllerGameObject().transform.Find("FloatingRemoteUI").GetChild(0).GetChild(2).gameObject;
            case 4:
                return deviceManager.GetControllerGameObject().transform.Find("FloatingRemoteUI").GetChild(0).GetChild(3).gameObject;
            case 5:
                return deviceManager.GetControllerGameObject().transform.Find("FloatingRemoteUI").GetChild(0).GetChild(4).gameObject;
            default:
                return null;
        }
    }

    // Passing the floating gameobject videoplayer - will be changed later for dynamic use
    public VideoPlayer GetButtonVideoPlayer(int objectNumber)
    {
        switch (objectNumber)
        {
            case 1:
                return GetVideoPlayerButtonObject(objectNumber).transform.GetChild(0).GetComponent<VideoPlayer>();
            case 2:
                return GetVideoPlayerButtonObject(objectNumber).transform.GetChild(0).GetComponent<VideoPlayer>();
            case 3:
                return GetVideoPlayerButtonObject(objectNumber).transform.GetChild(0).GetComponent<VideoPlayer>();
            case 4:
                return GetVideoPlayerButtonObject(objectNumber).transform.GetChild(0).GetComponent<VideoPlayer>();
            default:
                return null;
        }
    }

    // -------------------------------------------------------------
    // VideoPlayerButtonHandler Sync Functions
    // -------------------------------------------------------------

    // Update a specific floating video to the main video
    public void updateVideoTime(
        MediaPlayer defaultMediaPlayer,
        VideoPlayer toBeSyncedButtonVideoPlayer)
    {
        float currentMPT = defaultMediaPlayer.Control.GetCurrentTimeMs();
        float currentMPTinSeconds = currentMPT / 1000;
        double currentMPTconvertedToDouble = convertFloatToDouble(currentMPTinSeconds);

        // Syncs the floating videoplayer with the main mediaplayer
        toBeSyncedButtonVideoPlayer.time = currentMPTconvertedToDouble;
    }

    // -------------------------------------------------------------
    // Handy Functions
    // -------------------------------------------------------------

    // Converts a float var to a double var 
    private double convertFloatToDouble(float flt)
    {
        decimal dec = new decimal(flt);
        double db = (double)dec;
        return db;
    }

    // -------------------------------------------------------------
    // ??? Useless Functions ???
    // -------------------------------------------------------------

    // Video Player on Menu Button
    //private VideoPlayer
    //    remoteButtonVideoPlayer1,
    //    remoteButtonVideoPlayer2,
    //    remoteButtonVideoPlayer3,
    //    remoteButtonVideoPlayer4;

    ////  Updates all the button videoplayer times and floating quad videoplayer times.
    //public void updateAllVideoTimes(MediaPlayer mediaPlayer)
    //{
    //    updateVideoTime(mediaPlayer, remoteButtonVideoPlayer1, floatingQuadVideoPlayer1);
    //    //updateVideoTime(mediaPlayer, remoteMenuButtonVideoPlayer2, floatingMediaPlayer2);
    //    //updateVideoTime(mediaPlayer, remoteMenuButtonVideoPlayer3, floatingMediaPlayer3);
    //    //updateVideoTime(mediaPlayer, remoteMenuButtonVideoPlayer4, floatingMediaPlayer4);
    //}

    //public GameObject instantiateVideoPlayer(
    //        GameObject floatingVideoObject,
    //        float defaultFloatingVideoPositionX,
    //        float defaultFloatingVideoPositionY,
    //        float defaultFloatingVideoPositionZ)
    //{
    //    return Instantiate(floatingVideoObject, new Vector3(defaultFloatingVideoPositionX, defaultFloatingVideoPositionY, defaultFloatingVideoPositionZ), Quaternion.identity);
    //}

    //public void instantantiedQuadFloatingVideoPlayerObject(int objectNumber)
    //{

    //    switch (objectNumber)
    //    {
    //        case (1):
    //            instantantiedQuadFloatingVideoPlayerObject1 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab1, new Vector3(0.95f + defaultVideoFloatX, -0.8f + defaultVideoFloatY, 0.8f + defaultVideoFloatZ));
    //            break;
    //        case (2):
    //            instantantiedQuadFloatingVideoPlayerObject2 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab2, new Vector3(0.95f + defaultVideoFloatX, 0.8f + defaultVideoFloatY, 0.8f + defaultVideoFloatZ));
    //            break;
    //        case (3):
    //            instantantiedQuadFloatingVideoPlayerObject3 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab3, new Vector3(1.2f + defaultVideoFloatX, 0.3f + defaultVideoFloatY, 1.0f + defaultVideoFloatZ));
    //            break;
    //        case (4):
    //            instantantiedQuadFloatingVideoPlayerObject4 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab4, new Vector3(1.2f + defaultVideoFloatX, -0.3f + defaultVideoFloatY, 1.0f + defaultVideoFloatZ));
    //            break;
    //        default:
    //            break;
    //    }

    //}
}
