﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {

    // ++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ SceneHandler +++ ---- ++++
    // ++++++++++++++++++++++++++++++++++++++++

    // -------------------------------------------------------------
    // SceneHandler Variables
    // -------------------------------------------------------------

    public static string scene0 = "MainMenu";
    public static string scene1 = "VideoDemoScene";

    // -------------------------------------------------------------
    // SceneHandler Core Functions
    // -------------------------------------------------------------

    // Load the scene containing the Main Menu
    public void loadMainMenuScene() { LoadScene(scene0); }

    // Load the scene containing the Video Demo Scene
    public void loadMainDemoScene() { LoadScene(scene1); }

    // -------------------------------------------------------------
    // Static SceneHandler Properties
    // -------------------------------------------------------------

    /// <summary>
    /// The total number of currently loaded Scenes.
    /// </summary>
    /// <remarks>
    /// The number of currently loaded Scenes will be returned.
    /// </remarks>
    public static int SceneCount() { return SceneManager.sceneCount; }

    /// <summary>
    /// Number of Scenes in Build Settings.
    /// </summary>
    /// <remarks>
    /// The number of Scenes which have been added to the Build Settings.
    /// The Editor will contain Scenes that were open before entering playmode.
    /// </remarks>
    public static int SceneCountInBuildSettings() { return SceneManager.sceneCountInBuildSettings; }

    // -------------------------------------------------------------
    // Static SceneHandler Methods
    // -------------------------------------------------------------

    /// <summary>
    /// Gets the currently active Scene.
    /// </summary>
    /// <returns>
    /// The active Scene.
    /// </returns>
    /// <remarks>
    /// The currently active Scene is the Scene which will be used as the target for new GameObjects instantiated by scripts.
    /// Note the active scene has no impact on what scenes are rendered.
    /// See Also: https://docs.unity3d.com/ScriptReference/SceneManagement.Scene.html
    /// </remarks>
    public static Scene GetActiveScene() { return SceneManager.GetActiveScene(); }

    /// <summary>
    /// Get the Scene at index in the SceneManager's list of loaded Scenes.
    /// </summary>
    /// <returns>
    /// A reference to the Scene at the index specified.
    /// </returns>
    /// <param name="sceneIndex">
    /// Index of the Scene to get. Index must be greater than or equal to 0 and less than SceneHandler.sceneCount.
    /// </param>
    public static Scene GetSceneAt(int sceneIndex) { return SceneManager.GetSceneAt(sceneIndex); }

    /// <summary>
    /// Searches through the Scenes loaded for a Scene with the given name.
    /// </summary>
    /// <returns>
    /// A reference to the Scene, if valid.
    /// If not, an invalid Scene is returned.
    /// </returns>
    /// <remarks>
    /// The name has to be without the .unity extension.
    /// The name can be the last part of the name as displayed in the BuildSettings window in which case the first Scene that matches will be returned.
    /// The name can also the be path as displayed in the Build Settings (still without the .unity extension), in which case only the exact match will be returned.
    /// This is case insensitive.
    /// </remarks>
    /// <param name="sceneName">
    /// Name of Scene to find.
    /// </param>
    public static Scene GetSceneByName(string sceneName) { return SceneManager.GetSceneByName(sceneName); }

    /// <summary>
    /// Get a Scene struct from a build index.
    /// </summary>
    /// <returns>
    /// A reference to the Scene, if valid.
    /// If not, an invalid Scene is returned.
    /// </returns>
    /// <remarks>
    /// This method will return a valid Scene if a Scene has been added to the build settings at the given build index AND the Scene is loaded.
    /// If it has not been loaded yet the SceneManager cannot return a valid Scene.
    /// See Also: https://docs.unity3d.com/ScriptReference/SceneManagement.SceneManager.GetSceneAt.html
    /// </remarks>
    /// <param name="sceneIndex">
    /// Build index as shown in the Build Settings window.
    /// </param>
    public static Scene GetSceneByBuildIndex(int sceneIndex) { return SceneManager.GetSceneByBuildIndex(sceneIndex); }

    /// <summary>
    /// Loads the Scene by its name or index in Build Settings.
    /// </summary>
    /// <returns>
    /// A handle to the Scene being loaded.
    /// </returns>
    /// <remarks>
    /// Note: In most cases, to avoid pauses or performance hiccups while loading, you should use the asynchronous version of this command which is: LoadSceneAsync.
    /// 
    /// When using SceneHandler.LoadScene, the loading does not happen immediately, it completes in the next frame.
    /// This semi-asynchronous behavior can cause frame stuttering and can be confusing because load does not complete immediately.
    /// 
    /// Since loading is set to complete in the next rendered frame, calling SceneHandler.LoadScene forces all previous AsynOperations to complete, even if AsyncOperation.allowSceneActivation is set to false.
    /// This can be avoided by using LoadSceneAsync instead.
    /// 
    /// The given sceneName can either be the Scene name only, without the .unity extension, or the path as shown in the BuildSettings window still without the .unity extension.
    /// If only the Scene name is given this will load the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full path.
    /// 
    /// Note that sceneName is case insensitive, except when you load the scene from an AssetBundle.
    /// </remarks>
    /// <param name="sceneName">
    /// Name or path of the Scene to load.
    /// </param>
    public static void LoadScene(string sceneName) { SceneManager.LoadScene(sceneName); }

    /// <summary>
    /// Loads the Scene by its name or index in Build Settings.
    /// </summary>
    /// <returns>
    /// A handle to the Scene being loaded.
    /// </returns>
    /// <remarks>
    /// Note: In most cases, to avoid pauses or performance hiccups while loading, you should use the asynchronous version of this command which is: LoadSceneAsync.
    /// 
    /// When using SceneHandler.LoadScene, the loading does not happen immediately, it completes in the next frame.
    /// This semi-asynchronous behavior can cause frame stuttering and can be confusing because load does not complete immediately.
    /// 
    /// Since loading is set to complete in the next rendered frame, calling SceneHandler.LoadScene forces all previous AsynOperations to complete, even if AsyncOperation.allowSceneActivation is set to false.
    /// This can be avoided by using LoadSceneAsync instead.
    /// 
    /// The given sceneName can either be the Scene name only, without the .unity extension, or the path as shown in the BuildSettings window still without the .unity extension.
    /// If only the Scene name is given this will load the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full path.
    /// 
    /// Note that sceneName is case insensitive, except when you load the scene from an AssetBundle.
    /// </remarks>
    /// <param name="sceneIndex">
    /// Index of the Scene in the Build Settings to load.
    /// </param>
    public static void LoadScene(int sceneIndex) { SceneManager.LoadScene(sceneIndex); }

    /// <summary>
    /// Loads the Scene asynchronously in the background.
    /// </summary>
    /// <returns>
    /// Use the AsyncOperation to determine if the operation has completed.
    /// </returns>
    /// <remarks>
    /// he given Scene name can either be the full Scene path, the path shown in the Build Settings window or just the Scene name.
    /// If only the Scene name is given this will load the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full Scene path in the Build Settings.
    /// </remarks>
    /// <param name="sceneName">
    /// Name or path of the Scene to load.
    /// </param>
    public static void LoadSceneAsync(string sceneName) { SceneManager.LoadSceneAsync(sceneName); }

    /// <summary>
    /// Loads the Scene asynchronously in the background.
    /// </summary>
    /// <returns>
    /// Use the AsyncOperation to determine if the operation has completed.
    /// </returns>
    /// <remarks>
    /// he given Scene name can either be the full Scene path, the path shown in the Build Settings window or just the Scene name.
    /// If only the Scene name is given this will load the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full Scene path in the Build Settings.
    /// </remarks>
    /// <param name="sceneIndex">
    /// Index of the Scene in the Build Settings to load.
    /// </param>
    public static void LoadSceneAsync(int sceneIndex) { SceneManager.LoadSceneAsync(sceneIndex); }

    /// <summary>
    /// Set the Scene to be active.
    /// </summary>
    /// <returns>
    /// Returns false if the Scene is not loaded yet.
    /// </returns>
    /// <remarks>
    /// The active Scene is the Scene which will be used as the target for new GameObjects instantiated by scripts and from what scene the lighting settings are used.
    /// When you add a Scene additively (see LoadSceneMode.Additive), the first Scene is still kept as the active Scene.
    /// Use this to switch the active Scene to the Scene you want as the target.
    /// 
    /// There must always be one scene marked as the active scene.Note the active scene has no impact on what scenes are rendered.
    /// </remarks>
    /// <param name="scene">
    /// The Scene to be set.
    /// </param>
    public static void SetActiveScene(Scene scene) { SceneManager.SetActiveScene(scene); }

    /// <summary>
    /// Destroys all GameObjects associated with the given Scene and removes the Scene from the SceneManager.
    /// </summary>
    /// <returns>
    /// Use the AsyncOperation to determine if the operation has completed.
    /// </returns>
    /// <remarks>
    /// The given Scene name can either be the full Scene path, the path shown in the Build Settings window or just the Scene name.
    /// If only the Scene name is given this will unload the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full Scene path.
    /// 
    /// Examples of supported formats:
    /// "Scene1"
    /// "Scene2"
    /// "Scenes/Scene3"
    /// "Scenes/Others/Scene3"
    /// "Assets/scenes/others/scene3.unity"
    /// 
    /// Note: This is case-insensitive and due to it being async there are no guarantees about completion time.
    /// Note: Assets are currently not unloaded.
    /// In order to free up asset memory call Resources.UnloadUnusedAssets.
    /// Note: It is not possible to UnloadSceneAsync if there are no scenes to load.
    /// For example, a project that has a single scene cannot use this static member.
    /// </remarks>
    /// <param name="sceneName">
    /// Name or path of the Scene to unload.
    /// </param>
    public static void UnloadSceneAsync(string sceneName) { SceneManager.UnloadSceneAsync(sceneName); }

    /// <summary>
    /// Destroys all GameObjects associated with the given Scene and removes the Scene from the SceneManager.
    /// </summary>
    /// <returns>
    /// Use the AsyncOperation to determine if the operation has completed.
    /// </returns>
    /// <remarks>
    /// The given Scene name can either be the full Scene path, the path shown in the Build Settings window or just the Scene name.
    /// If only the Scene name is given this will unload the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full Scene path.
    /// 
    /// Examples of supported formats:
    /// "Scene1"
    /// "Scene2"
    /// "Scenes/Scene3"
    /// "Scenes/Others/Scene3"
    /// "Assets/scenes/others/scene3.unity"
    /// 
    /// Note: This is case-insensitive and due to it being async there are no guarantees about completion time.
    /// Note: Assets are currently not unloaded.
    /// In order to free up asset memory call Resources.UnloadUnusedAssets.
    /// Note: It is not possible to UnloadSceneAsync if there are no scenes to load.
    /// For example, a project that has a single scene cannot use this static member.
    /// </remarks>
    /// <param name="sceneIndex">
    /// Index of the Scene in BuildSettings.
    /// </param>
    public static void UnloadSceneAsync(int sceneIndex) { SceneManager.UnloadSceneAsync(sceneIndex); }

    /// <summary>
    /// Destroys all GameObjects associated with the given Scene and removes the Scene from the SceneManager.
    /// </summary>
    /// <returns>
    /// Use the AsyncOperation to determine if the operation has completed.
    /// </returns>
    /// <remarks>
    /// The given Scene name can either be the full Scene path, the path shown in the Build Settings window or just the Scene name.
    /// If only the Scene name is given this will unload the first Scene in the list that matches.
    /// If you have multiple Scenes with same name but different paths, you should use the full Scene path.
    /// 
    /// Examples of supported formats:
    /// "Scene1"
    /// "Scene2"
    /// "Scenes/Scene3"
    /// "Scenes/Others/Scene3"
    /// "Assets/scenes/others/scene3.unity"
    /// 
    /// Note: This is case-insensitive and due to it being async there are no guarantees about completion time.
    /// Note: Assets are currently not unloaded.
    /// In order to free up asset memory call Resources.UnloadUnusedAssets.
    /// Note: It is not possible to UnloadSceneAsync if there are no scenes to load.
    /// For example, a project that has a single scene cannot use this static member.
    /// </remarks>
    /// <param name="scene">
    /// Scene to unload.
    /// </param>
    public static void UnloadSceneAsync(Scene scene) { SceneManager.UnloadSceneAsync(scene); }
    
}
