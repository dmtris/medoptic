﻿using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;

public class MenuHandler : MonoBehaviour
{
    // +++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ MenuHandler +++ ---- ++++
    // +++++++++++++++++++++++++++++++++++++++

    /* 
     * NOTES: 
     * The OVRCameraRig needs to be set to active or the project will error out.
     * If not, you will recieve a "object reference not set to an instance of an object" error.
     * 
     */

    // -------------------------------------------------------------
    // MenuHandler Variables
    // -------------------------------------------------------------

    private SpawnAtRuntime spawnAtRuntime;
    private DeviceManager deviceManager;
    private VideoPlayerHandler videoPlayerHandler;
    private VideoPlayerButtonHandler videoPlayerButtonHandler;

    private GameObject remoteMenu;

    private bool remoteMenuIsVisible;

    private Text showAllButtonText;

    private Button
        menuButton1,
        menuButton2,
        menuButton3,
        menuButton4,
        menuButtonShowAll;

    // Notifier used to check if a script has been run once already
    private bool buttonMenuExists = false;

    // current menu position 
    private int currentMenuPosition = 1;

    // total amount of menu items  
    private int maxMenuPosition = 5;

    // -------------------------------------------------------------
    // FloatingOptionsMenu Variables
    // -------------------------------------------------------------

    private Toggle
        threeDVideoToggle,
        miosToggle,
        muteToggle;

    private Slider
        volumeSlider,
        balanceSlider,
        posXSlider,
        posYSlider,
        posZSlider,
        rotXSlider,
        rotYSlider;

    private float
        defaultRotX,
        defaultRotY,
        defaultRotZ,
        currentRotX,
        currentRotY,
        currentRotZ;

    // -------------------------------------------------------------
    // MenuHandler Core Functions
    // -------------------------------------------------------------

    public void Awake()
    {
        // Get the SpawnAtRuntime script from the current GameObject
        spawnAtRuntime = gameObject.GetComponent<SpawnAtRuntime>();

        // Get the DeviceManager script from the current GameObject
        deviceManager = gameObject.GetComponent<DeviceManager>();

        // Get the VideoPlayerHandler script fromt he current GameObject
        videoPlayerHandler = gameObject.GetComponent<VideoPlayerHandler>();

        // Get the 3DVideoObj Toggle Obj.
        threeDVideoToggle = deviceManager.GetFloatingOptionsMenuChildObj(0).GetComponent<Toggle>();

        // Get the MIOs Toggle Obj.
        miosToggle = deviceManager.GetFloatingOptionsMenuChildObj(1).GetComponent<Toggle>();

        // Get the Volume Slider Obj
        volumeSlider = deviceManager.GetFloatingOptionsMenuChildObj(2).GetComponent<Slider>();

        // Get the Balance Slider Obj
        balanceSlider = deviceManager.GetFloatingOptionsMenuChildObj(3).GetComponent<Slider>();

        // Get the Mute Toggle Obj
        muteToggle = deviceManager.GetFloatingOptionsMenuChildObj(4).GetComponent<Toggle>();

        // Get the Position XSlider Slider Obj
        posXSlider = deviceManager.GetFloatingOptionsMenuChildObj(5).GetComponent<Slider>();

        // Get the Position YSlider Slider Obj
        posYSlider = deviceManager.GetFloatingOptionsMenuChildObj(6).GetComponent<Slider>();

        // Get the Position ZSlider Slider Obj
        posZSlider = deviceManager.GetFloatingOptionsMenuChildObj(7).GetComponent<Slider>();

        // Get the Rotation XSlider Slider Obj
        rotXSlider = deviceManager.GetFloatingOptionsMenuChildObj(8).GetComponent<Slider>();

        // Get the Rotation YSlider Slider Obj
        rotYSlider = deviceManager.GetFloatingOptionsMenuChildObj(9).GetComponent<Slider>();
    }

    // Use this for initialization
    void Start()
    {
        //Add listener for when the state of the 3DVideoToggle changes, to take action
        threeDVideoToggle.onValueChanged.AddListener(delegate {
            ThreeDVideoToggleValueChanged(threeDVideoToggle);
        });

        //Add listener for when the state of the miosToggle changes, to take action
        miosToggle.onValueChanged.AddListener(delegate {
            MIOsToggleValueChanged(miosToggle);
        });

        //Adds a listener to the volumeSlider slider and invokes a method when the value changes.
        volumeSlider.onValueChanged.AddListener(delegate { VolumeSliderValueChangeCheck(volumeSlider); });

        //Adds a listener to the balanceSlider slider and invokes a method when the value changes.
        balanceSlider.onValueChanged.AddListener(delegate { BalanceSliderValueChangeCheck(balanceSlider); });

        //Add listener for when the state of the muteToggle changes, to take action
        muteToggle.onValueChanged.AddListener(delegate {
            MuteToggleValueChanged(muteToggle);
        });

        // Getting the default values of the 360SphereVideo GameObject's rotation;
        defaultRotX = deviceManager.GetSphereVideoObj().transform.parent.transform.rotation.eulerAngles.x;
        defaultRotY = deviceManager.GetSphereVideoObj().transform.parent.transform.rotation.eulerAngles.y;
        defaultRotZ = deviceManager.GetSphereVideoObj().transform.parent.transform.rotation.eulerAngles.z;

        //Adds a listener to the posXSlider slider and invokes a method when the value changes.
        posXSlider.onValueChanged.AddListener(delegate { PositionXSliderValueChangeCheck(posXSlider); });

        //Adds a listener to the posYSlider slider and invokes a method when the value changes.
        posYSlider.onValueChanged.AddListener(delegate { PositionYSliderValueChangeCheck(posYSlider); });

        //Adds a listener to the posZSlider slider and invokes a method when the value changes.
        posZSlider.onValueChanged.AddListener(delegate { PositionZSliderValueChangeCheck(posZSlider); });

        //Adds a listener to the rotXSlider slider and invokes a method when the value changes.
        rotXSlider.onValueChanged.AddListener(delegate { RotationXSliderValueChangeCheck(rotXSlider); });

        //Adds a listener to the rotYSlider slider and invokes a method when the value changes.
        rotYSlider.onValueChanged.AddListener(delegate { RotationYSliderValueChangeCheck(rotYSlider); });
    }

    // Update is called once per frame
    void Update()
    {
        // Getting the current values of the 360SphereVideo GameObject's rotation;
        currentRotX = deviceManager.GetSphereVideoObj().transform.parent.transform.rotation.eulerAngles.x;
        currentRotY = deviceManager.GetSphereVideoObj().transform.parent.transform.rotation.eulerAngles.y;
        currentRotZ = deviceManager.GetSphereVideoObj().transform.parent.transform.rotation.eulerAngles.z;

        if (deviceManager.GetControllerGameObject() != null)
        {
            if (!buttonMenuExists && spawnAtRuntime.GetRemoteMenuNotifierValue())
            {
                // Get the VideoPlayerButtonHandler script fromt he current GameObject
                videoPlayerButtonHandler = gameObject.GetComponent<VideoPlayerButtonHandler>();

                // Get the FloatingRemoteUI GameObject
                remoteMenu = deviceManager.GetControllerGameObject().transform.Find("FloatingRemoteUI").gameObject;

                // Get the FloatingRemoteUI Buttons
                menuButton1 = remoteMenu.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Button>();
                menuButton2 = remoteMenu.transform.GetChild(0).GetChild(1).gameObject.GetComponent<Button>();
                menuButton3 = remoteMenu.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Button>();
                menuButton4 = remoteMenu.transform.GetChild(0).GetChild(3).gameObject.GetComponent<Button>();
                menuButtonShowAll = remoteMenu.transform.GetChild(0).GetChild(4).gameObject.GetComponent<Button>();

                showAllButtonText = remoteMenu.transform.GetChild(0).GetChild(maxMenuPosition - 1).GetChild(0).GetChild(0).GetComponent<Text>();

                // Hide menu on start
                if (remoteMenu.GetComponent<Canvas>().enabled)
                {
                    remoteMenuIsVisible = false;
                    remoteMenu.GetComponent<Canvas>().enabled = false;
                }

                buttonMenuExists = true;
            }

            if (buttonMenuExists)
            {
                if (videoPlayerHandler.AreAllVideosVisible())
                {
                    showAllButtonText.text = "Hide";
                }
                else
                {
                    showAllButtonText.text = "Show";
                }
            }
        }
    }

    // -------------------------------------------------------------
    // MenuHandler Get / Set Functions
    // -------------------------------------------------------------

    // Get the RemoteMenu GameObject
    public GameObject GetRemoteMenuObject() { return remoteMenu; }

    // Get the Maximum Position Available for the menu.
    public int GetMaxMenuPosition() { return maxMenuPosition; }

    // Get the ShowAll Button Text
    public Text GetShowAllButtonText() { return showAllButtonText; }



    // -------------------------------------------------------------
    // FloatingOptionsMenu Listener Functions
    // -------------------------------------------------------------

    // Output the new state of the 3DVideoToggle
    private void ThreeDVideoToggleValueChanged(Toggle toggle)
    {
        if (toggle.isOn)
        {
            // Do something
            deviceManager.GetSphereVideoObj().GetComponent<MeshRenderer>().enabled = true;
        } else
        {
            // Do something
            deviceManager.GetSphereVideoObj().GetComponent<MeshRenderer>().enabled = false;
        }
    }

    // Output the new state of the miosToggle
    private void MIOsToggleValueChanged(Toggle toggle)
    {
        if (toggle.isOn)
        {
            // Do something
            ShowFloatingQuadVideo(1, spawnAtRuntime.GetMediaPlayer());
            ShowFloatingQuadVideo(2, spawnAtRuntime.GetMediaPlayer());
            ShowFloatingQuadVideo(3, spawnAtRuntime.GetMediaPlayer());
            ShowFloatingQuadVideo(4, spawnAtRuntime.GetMediaPlayer());

        }
        else
        {
            // Do something
            HideFloatingQuadVideo(1);
            HideFloatingQuadVideo(2);
            HideFloatingQuadVideo(3);
            HideFloatingQuadVideo(4);
        }
    }

    // Output the new state of the audio Volume Slider
    private void VolumeSliderValueChangeCheck(Slider slider)
    {
        MediaPlayer mPlayer = deviceManager.GetSphereVideoObj().transform.parent.transform.GetChild(1).gameObject.GetComponent<MediaPlayer>();
        mPlayer.Control.SetVolume(slider.value);
    }

    // Output the new state of the audio Balance Slider
    private void BalanceSliderValueChangeCheck(Slider slider)
    {
        MediaPlayer mPlayer = deviceManager.GetSphereVideoObj().transform.parent.transform.GetChild(1).gameObject.GetComponent<MediaPlayer>();
        mPlayer.Control.SetBalance(slider.value);
    }

    // Output the new state of the Mute Toggle
    private void MuteToggleValueChanged(Toggle toggle)
    {
        MediaPlayer mPlayer = deviceManager.GetSphereVideoObj().transform.parent.transform.GetChild(1).gameObject.GetComponent<MediaPlayer>();
        if (toggle.isOn) { mPlayer.Control.MuteAudio(true); }
        else { mPlayer.Control.MuteAudio(false); }
    }

    // Output the new state of the Position XSlider
    private void PositionXSliderValueChangeCheck(Slider slider)
    {
        GameObject sphere = deviceManager.GetSphereVideoObj().transform.parent.gameObject;
        sphere.transform.position = new Vector3((slider.value / 5), sphere.transform.position.y, sphere.transform.position.z);
    }

    // Output the new state of the Position YSlider
    private void PositionYSliderValueChangeCheck(Slider slider)
    {
        GameObject sphere = deviceManager.GetSphereVideoObj().transform.parent.gameObject;
        sphere.transform.position = new Vector3(sphere.transform.position.x, (slider.value / 5), sphere.transform.position.z);
    }

    // Output the new state of the Position ZSlider
    private void PositionZSliderValueChangeCheck(Slider slider)
    {
        GameObject sphere = deviceManager.GetSphereVideoObj().transform.parent.gameObject;
        sphere.transform.position = new Vector3(sphere.transform.position.x, sphere.transform.position.y, (slider.value / 5));
    }

    // Output the new state of the Rotation XSlider
    private void RotationXSliderValueChangeCheck(Slider slider)
    {
        GameObject sphere = deviceManager.GetSphereVideoObj().transform.parent.gameObject;
        sphere.transform.rotation = Quaternion.Euler(new Vector3(defaultRotX + (5 * slider.value), currentRotY, currentRotZ));
    }

    // Output the new state of the Rotation YSlider
    private void RotationYSliderValueChangeCheck(Slider slider)
    {
        GameObject sphere = deviceManager.GetSphereVideoObj().transform.parent.gameObject;
        sphere.transform.rotation = Quaternion.Euler(new Vector3(currentRotX, defaultRotY + (5 * slider.value), currentRotZ));
    }

    // -------------------------------------------------------------
    // MenuHandler Handy Functions
    // -------------------------------------------------------------

    public void activateButtonMethod()
    {
        // If clicking showAll/hideAll button
        if (currentMenuPosition == maxMenuPosition)
        {
            if (showAllButtonText.text == "Show")
            {
                // run show all floating quad videos
                ShowFloatingQuadVideo(1, spawnAtRuntime.GetMediaPlayer());
                ShowFloatingQuadVideo(2, spawnAtRuntime.GetMediaPlayer());
                ShowFloatingQuadVideo(3, spawnAtRuntime.GetMediaPlayer());
                ShowFloatingQuadVideo(4, spawnAtRuntime.GetMediaPlayer());

            }
            if (showAllButtonText.text == "Hide")
            {
                HideFloatingQuadVideo(1);
                HideFloatingQuadVideo(2);
                HideFloatingQuadVideo(3);
                HideFloatingQuadVideo(4);
            }
        }
        else
        {
            // open specific floating quad video 
            ShowFloatingQuadVideo(currentMenuPosition, spawnAtRuntime.GetMediaPlayer());
        }
    }

    // Shows or moves selected floating quad video
    public void ShowFloatingQuadVideo(int menuPosition, MediaPlayer mediaPlayer)
    {
        if (menuPosition < maxMenuPosition)
        {
            // updates video times 
            // TODO  ( only actually updates endoscopy video right now because navigator videos are not full.. )
            if (menuPosition == 1) { spawnAtRuntime.updateAllVideoTimes(mediaPlayer); }

            // Play selected floating video quad
            videoPlayerHandler.GetFloatingQuadVideoPlayer(menuPosition).Play();

            // Is GameObject Visible?
            bool gameObjectIsVisble = videoPlayerHandler.GetFloatingVideoPlayerObj(menuPosition).GetComponent<MeshRenderer>().enabled; ;

            // Show the gameobject if it wasnt already visible
            if (!gameObjectIsVisble) { videoPlayerHandler.GetFloatingVideoPlayerObj(menuPosition).GetComponent<MeshRenderer>().enabled = true; }

            // Reset its position in space
            spawnAtRuntime.ResetFloatingQuadVideoPlayerObjPos(menuPosition);

            // Increase scale back to default (scale was 0 to hide it)
            videoPlayerHandler.GetFloatingQuadVideoPlayer(menuPosition).transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
    }

    public void HideFloatingQuadVideo(int menuPosition)
    {
        if (menuPosition < GetMaxMenuPosition()) // This is specifying not to do something on showall button.. 
        {

            // Pause floating video quad being hidden
            videoPlayerHandler.GetFloatingQuadVideoPlayer(menuPosition).Pause();

            // Is GameObject Visible
            bool gameObjectIsVisble = videoPlayerHandler.GetFloatingVideoPlayerObj(menuPosition).GetComponent<MeshRenderer>().enabled; ;

            // Hide the gameobject if it wasnt already hidden
            if (gameObjectIsVisble) { videoPlayerHandler.GetFloatingVideoPlayerObj(menuPosition).GetComponent<MeshRenderer>().enabled = false; }

            // Move back to origin
            spawnAtRuntime.ResetFloatingQuadVideoPlayerObjPos(menuPosition);

            // Set scale to 0 to not interfere with reticle
            videoPlayerHandler.GetFloatingQuadVideoPlayer(menuPosition).transform.localScale = new Vector3(0f, 0f, 0f);
            //Debug.Log("hidden");
        }
    }

    //Toggles the controller menu
    public void MenuToggle()
    {
        // Toggle remoteMenu visibility using menu tracker
        if (!remoteMenuIsVisible)
        {
            // plays video textures
            if (!videoPlayerButtonHandler.GetButtonVideoPlayer(1).isPlaying) { videoPlayerButtonHandler.GetButtonVideoPlayer(1).Play(); }
            if (!videoPlayerButtonHandler.GetButtonVideoPlayer(2)) { videoPlayerButtonHandler.GetButtonVideoPlayer(2).Play(); }
            if (!videoPlayerButtonHandler.GetButtonVideoPlayer(3)) { videoPlayerButtonHandler.GetButtonVideoPlayer(3).Play(); }
            if (!videoPlayerButtonHandler.GetButtonVideoPlayer(4)) { videoPlayerButtonHandler.GetButtonVideoPlayer(4).Play(); }

            // Set video times: 
            spawnAtRuntime.updateAllVideoTimes(spawnAtRuntime.GetMediaPlayer());

            // Set Menu tracker visibility to true
            remoteMenuIsVisible = true;

            // Show the menu
            remoteMenu.GetComponent<Canvas>().enabled = true;

            // Resets menu position
            currentMenuPosition = 1;
            menuItemSelect(menuButton1);

        }
        else
        {
            // Set Menu tracker visibility to false
            remoteMenuIsVisible = false;
            // Hide the menu
            remoteMenu.GetComponent<Canvas>().enabled = false;

            menuItemSelect(menuButton1);
            // pauses video textures
            if (videoPlayerButtonHandler.GetButtonVideoPlayer(1).isPlaying) { videoPlayerButtonHandler.GetButtonVideoPlayer(1).Pause(); }
            if (videoPlayerButtonHandler.GetButtonVideoPlayer(2)) { videoPlayerButtonHandler.GetButtonVideoPlayer(2).Pause(); }
            if (videoPlayerButtonHandler.GetButtonVideoPlayer(3)) { videoPlayerButtonHandler.GetButtonVideoPlayer(3).Pause(); }
            if (videoPlayerButtonHandler.GetButtonVideoPlayer(4)) { videoPlayerButtonHandler.GetButtonVideoPlayer(4).Pause(); }

        }
        // The old way to toggle the menu.
        // remoteMenu.GetComponent<Canvas>().enabled = !remoteMenu.GetComponent<Canvas>().enabled;
    }

    // handles menu item selection direction 
    public void MenuItemSwipe(float direction)
    {
        // menu direction left
        if (direction < 0)
        {
            //Debug.Log("menu left");

            currentMenuPosition--;
            if (currentMenuPosition < 1)
            {
                currentMenuPosition = maxMenuPosition;
            }

        }
        // menu dirction right 
        else
        {
            //Debug.Log("menu right");

            currentMenuPosition++;
            if (currentMenuPosition > maxMenuPosition)
            {
                currentMenuPosition = 1;
            }
        }

        menuItemSelect(determineButtonByMenuPosition(currentMenuPosition));
    }


    private Button determineButtonByMenuPosition(int position)
    {

        Button currentRemoteMenuButton;

        switch (position)
        {
            case 1:
                currentRemoteMenuButton = menuButton1;
                break;
            case 2:
                currentRemoteMenuButton = menuButton2;
                break;
            case 3:
                currentRemoteMenuButton = menuButton3;
                break;
            case 4:
                currentRemoteMenuButton = menuButton4;
                break;
            case 5:
                currentRemoteMenuButton = menuButtonShowAll;
                break;
            default:
                currentRemoteMenuButton = menuButton1;
                break;
        }

        return currentRemoteMenuButton;
    }

    private void menuItemSelect(Button remoteButton)
    {
        // deselect all 
        menuButton1.OnDeselect(null);
        menuButton2.OnDeselect(null);
        menuButton3.OnDeselect(null);
        menuButton4.OnDeselect(null);
        menuButtonShowAll.OnDeselect(null);

        remoteButton.OnSelect(null);
    }

    public void SyncAllVideoTimes() { spawnAtRuntime.updateAllVideoTimes(spawnAtRuntime.GetMediaPlayer()); }
}
