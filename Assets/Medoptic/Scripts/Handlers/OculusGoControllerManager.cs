﻿using UnityEngine;
using UnityEngine.UI;
using EasyInputVR.StandardControllers;

public class OculusGoControllerManager : MonoBehaviour
{
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ OculusGoControllerManager +++ ---- ++++
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++

    // -------------------------------------------------------------
    // OculusGoControllerManager Variables
    // -------------------------------------------------------------

    private MenuHandler menuHandler;
    private VideoPlayerHandler videoPlayerHandler;
    private RaycastHandler raycastHandler;
    private DeviceManager deviceManager;

    private GameObject
        hitObject = null;

    private Vector3
        initialScale;

    private bool
        primaryTouchpadButtonPressed = false;
        // primaryIndexTriggerPressed = false;

    public float
        primaryTriggerAxisHoldTime = 0.5f,
        swipeValueDeadZone = 0.3f;

    private float
        previousAxisValueOfX = 0,
        initialAxisValueOfY = 0,
        currentAxisValueOfY = 0, 
        xAxis,
        yAxis,
        quadScaleModifier,
        minScaleSize,
        maxScaleSize;

    // -------------------------------------------------------------
    // OculusGoControllerManager Core Functions
    // -------------------------------------------------------------

    private void Awake()
    {
        // Get the MenuHandler script via SpawnAtRuntime
        menuHandler = GetSpawnAtRuntimeContainer().gameObject.GetComponent<MenuHandler>();

        // Get the VideoPlayerHandler script via SpawnAtRuntime
        videoPlayerHandler = GetSpawnAtRuntimeContainer().gameObject.GetComponent<VideoPlayerHandler>();

        // Get the RaycastHandler script attached to the same GameObject
        raycastHandler = gameObject.GetComponent<RaycastHandler>();

        // Get the DeviceManager script attached to the SpawnAtRuntime GameObject
        deviceManager = GameObject.Find("/SpawnAtRuntime").GetComponent<DeviceManager>();

        // Setting Variables to default values
        initialScale = new Vector3(0, 0, 0);

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            Debug.Log("backButton pressed");

            // MARK - Hacky Way To Update Video Times
            menuHandler.SyncAllVideoTimes();

            // open or close the remoteMenu 
            // menuHandler.MenuToggle();
        }
        
        // Handles the scale functionality
        if (raycastHandler.IsHittingFloatingQuad())
        {
            hitObject = raycastHandler.GetHitObject().collider.gameObject;
            yAxis = -(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y);

            if( hitObject.GetComponent<StandardGrabReceiver>().getAllowSwipeForScale() &&
                hitObject.GetComponent<StandardGrabReceiver>().getGrabModePrimary() &&
                OVRInput.Get(OVRInput.Touch.PrimaryTouchpad))
            {
                quadScaleModifier = hitObject.GetComponent<StandardGrabReceiver>().getSwipeScaleModifier();
                minScaleSize = hitObject.GetComponent<StandardGrabReceiver>().getMinScaleSize();
                maxScaleSize = hitObject.GetComponent<StandardGrabReceiver>().getMaxScaleSize();

                // Gets the initial first time thumb presses down on touchpad.
                // Note: turns to false immediately afterwards even if thumb is still on touchpad.
                if (OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad))
                {


                    // Set the initial Scale Value;
                    initialScale = raycastHandler.GetHitObject().transform.localScale;

                    // Set the initial Axis Value Of Y
                    initialAxisValueOfY = yAxis;

                } else
                {
                    float floatDiff = yAxis - initialAxisValueOfY;
                    float newScaleSize = initialScale.x + (floatDiff * quadScaleModifier); 

                    if (newScaleSize < minScaleSize) { newScaleSize = minScaleSize; }
                    else if (newScaleSize > maxScaleSize) { newScaleSize = maxScaleSize; } 

                    raycastHandler.GetHitObject().transform.localScale = new Vector3(newScaleSize, newScaleSize, newScaleSize);
             
                }
            } else
            {
                // Resetting Variables
                quadScaleModifier = 1;
                minScaleSize = 0.5f;
                maxScaleSize = 0.5f;
                initialScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
        } else
        {
            // Resetting Variables
            hitObject = null;
            yAxis = 0;
            initialAxisValueOfY = 0;
        }
    }

    // -------------------------------------------------------------
    // OculusGoControllerManager Get / Set Functions
    // -------------------------------------------------------------

    // Return the SpawnAtRuntime gameobject
    public GameObject GetSpawnAtRuntimeContainer() { return GameObject.Find("/SpawnAtRuntime"); }

    // -------------------------------------------------------------
    // ??? Useless Functions ???
    // -------------------------------------------------------------

    // TEMPORARILY REMOVED.
    //var primaryIndexTriggerAxis = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger);

    //// If the trigger was not already being pressed..
    //if (!primaryIndexTriggerPressed)
    //{

    //    // If trigger is pressed..
    //    if (primaryIndexTriggerAxis > .5)
    //    {
    //        if (menuManager.remoteMenu.GetComponent<Canvas>().enabled)
    //        {
    //            Debug.Log("trigger pressed. init floating quad if menu open");
    //            menuManager.activateButtonMethod();
    //        }
    //        else
    //        {
    //            Debug.Log("trigger pressed but menu is closed");
    //        }
    //        primaryIndexTriggerPressed = true;
    //    }
    //}
    //// Reset primaryIndexTriggerPressed 
    //if (primaryIndexTriggerAxis == 0)
    //{
    //    primaryIndexTriggerPressed = false;
    //}


    // var primaryPrimaryTouchpadButton = OVRInput.Get(OVRInput.Button.One);

    //private bool IsRaycastHittingFloatingMenu()
    //{
    //    var instantantiedFloatingVideoQuad1 = videoPlayerHandler.GetFloatingVideoPlayerObj(1);
    //    var instantantiedFloatingVideoQuad2 = videoPlayerHandler.GetFloatingVideoPlayerObj(2);
    //    var instantantiedFloatingVideoQuad3 = videoPlayerHandler.GetFloatingVideoPlayerObj(3);
    //    var instantantiedFloatingVideoQuad4 = videoPlayerHandler.GetFloatingVideoPlayerObj(4);

    //    bool result;

    //    // If the currently hovered flaoting quad equals prefab name determine which menu position to select
    //    if (raycastHandler.GetHitObject().transform.gameObject.name == (instantantiedFloatingVideoQuad2.gameObject.name.ToString())) { result = true; }
    //    else if (raycastHandler.GetHitObject().transform.gameObject.name == (instantantiedFloatingVideoQuad2.gameObject.name.ToString())) { result = true; }
    //    else if (raycastHandler.GetHitObject().transform.gameObject.name == (instantantiedFloatingVideoQuad3.gameObject.name.ToString())) { result = true; }
    //    else if (raycastHandler.GetHitObject().transform.gameObject.name == (instantantiedFloatingVideoQuad4.gameObject.name.ToString())) { result = true; }
    //    else { result = false; }

    //    return result;
    //}

    //// Does same function as the trigger.
    //// Selects a item from the menu.
    //// However doesn't grab the item
    //if (OVRInput.GetUp(OVRInput.Button.One) && !primaryTouchpadButtonPressed && !raycastHandler.IsHittingFloatingQuad())
    //{
    //    if (menuHandler.GetRemoteMenuObject().GetComponent<Canvas>().enabled)
    //    {
    //        Debug.Log("touchpad button pressed. init floating quad if menu open");
    //        menuHandler.activateButtonMethod();
    //    }
    //    else { Debug.Log("touchpad button pressed but menu is closed"); }

    //    primaryTouchpadButtonPressed = true;
    //}
    //else if (OVRInput.GetDown(OVRInput.Button.One) && !primaryTouchpadButtonPressed && raycastHandler.IsHittingFloatingQuad())
    //{ 
    //        primaryTouchpadButtonPressed = true;
    //}

    //if (OVRInput.GetUp(OVRInput.Button.One)) { primaryTouchpadButtonPressed = false; }
        
    //// If TouchDown get the current float of x axis and set it to a float
    //if (OVRInput.GetDown(OVRInput.Touch.PrimaryTouchpad))
    //{
    //    Debug.Log("!!!!! TOUCH DOWN !!!!!");
    //    previousAxisValueOfX = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x;
    //}
    //// Else check if its TouchUp.
    //// If TouchUp take the previous float and compare it to the new float of x axis.
    //else if (OVRInput.GetUp(OVRInput.Touch.PrimaryTouchpad))
    //{
    //    // Default float value
    //    float floatDiff = 0;
    //    xAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x;

    //    Debug.Log("!!!!! TOUCH UP !!!!!");
    //    // If previous float is less then the current.
    //    // Check if you are preforming a valid swipe right gesture.
    //    if (previousAxisValueOfX<xAxis)
    //    {
    //        // Calulate float difference
    //        floatDiff = xAxis - previousAxisValueOfX;

    //        // Check if float difference greater >= dead zone value.
    //        // Then it is a valid swipe gesture.
    //        if (floatDiff >= swipeValueDeadZone)
    //        {
    //            Debug.Log("SWIPED RIGHT");

    //            // Menu Direction Event
    //            menuHandler.MenuItemSwipe(1);
    //        }
    //        // Else it is not a valid swipe gesture.
    //        else { Debug.Log("NOT SWIPED RIGHT: DEAD ZONE"); }
    //    }
    //    // Else check if you are performing a valid swipe left gesture.
    //    else
    //    {
    //        // Calulate float difference
    //        floatDiff = previousAxisValueOfX - xAxis;

    //        // Check if float difference greater >= dead zone value.
    //        // Then it is a valid swipe gesture.
    //        if (floatDiff >= swipeValueDeadZone)
    //        {
    //            Debug.Log("SWIPED LEFT");

    //            // Menu Direction Event
    //            menuHandler.MenuItemSwipe(-1);
    //        }
    //        // Else it is not a valid swipe gesture.
    //        else { Debug.Log("NOT SWIPED LEFT: DEAD ZONE"); }
    //    }
    //    // Resetting Variables
    //    previousAxisValueOfX = 0;
    //}

}
