﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingButtonHandler : MonoBehaviour {

    private GameObject buttonGameObj;
    private Button buttonObj;
    public Sprite startSprite;
    public Sprite endSprite;
    public GameObject imageGameObj;
    public GameObject menuObj;
    private Image imageObj;
    private bool state = true;

    private void Awake()
    {
        buttonGameObj = this.gameObject;
        buttonObj = buttonGameObj.GetComponent<Button>();
        imageObj = imageGameObj.GetComponent<Image>();
        if (menuObj != null) { menuObj.SetActive(!state); }
    }

    // Use this for initialization
    void Start ()
    {
        buttonObj.onClick.AddListener(swapBoolValue);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void swapBoolValue()
    {
        if(!state)
        {
            imageObj.sprite = endSprite;
            
        } else
        {
            imageObj.sprite = startSprite;
        }

        if (menuObj != null)
        {
            if (menuObj.activeSelf != state)
            {
                menuObj.SetActive(state);
            }
        }

        state = !state;
    }
}
