﻿using UnityEngine;
using UnityEngine.UI;
public class RaycastHandler : MonoBehaviour
{
    // ++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ RaycastHandler +++ ---- ++++
    // ++++++++++++++++++++++++++++++++++++++++++

    // -------------------------------------------------------------
    // RaycastHandler Variables
    // -------------------------------------------------------------

    private bool lazerFound = false;
    //private bool isHittingCurvedMenu = false;
    private bool isHittingFloatingQuad = false;
    private bool isHittingFloatingMenu = false;
    private RaycastHit hitObject;
    private LineRenderer lazer;

    // -------------------------------------------------------------
    // RaycastHandler Core Functions
    // -------------------------------------------------------------

    private void Awake()
    {
        // deviceManager = GameObject.Find("/SpawnAtRuntime").GetComponent<DeviceManager>();
    }

    // Use this for initialization
    void Start ()
    {

    }

	// Update is called once per frames
	void Update () {

        if(gameObject.GetComponent<LineRenderer>() != null && !lazerFound)
        {
            lazer = gameObject.GetComponent<LineRenderer>();
            lazer.sortingOrder = 10;
            lazerFound = true;
        }

        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;


        if (SceneHandler.GetActiveScene().name != SceneHandler.scene0)
        {
            if (Physics.Raycast(transform.position, forward, out hitObject))
            {
                // If raycast hits a FloatingQuad and it's showing
                if (hitObject.collider.tag == "FloatingQuad" && hitObject.collider.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    if (hitObject.collider.gameObject.GetComponent<MeshRenderer>().enabled)
                    {
                        // OLD WAY
                        // if (lazerFound) { lazer.enabled = true; }
                        // NEW WAY
                        if(lazerFound)
                        {
                            lazer.startColor = ColorCoordinator.laserBlue(ColorCoordinator.alphaSolid);
                            lazer.endColor = ColorCoordinator.laserBlue(ColorCoordinator.alphaEmpty);
                        }
                        isHittingFloatingQuad = true;
                        return;
                    }
                }
                // If raycast hits a FloatingMenu and it's showing
                else if (hitObject.collider.tag == "FloatingMenu" && hitObject.collider.gameObject != null)
                {
                    if(hitObject.collider.gameObject.activeInHierarchy == true)
                    {
                        // OLD WAY
                        // if (lazerFound) { lazer.enabled = true; }
                        // NEW WAY
                        if (lazerFound)
                        {
                            lazer.startColor = ColorCoordinator.laserBlue(ColorCoordinator.alphaSolid);
                            lazer.endColor = ColorCoordinator.laserBlue(ColorCoordinator.alphaEmpty);
                        }
                        isHittingFloatingMenu = true;
                        return;
                    }
                }

            }
            else
            {
                // OLD WAY
                // if (lazerFound) { lazer.enabled = false; }
                // NEW WAY
                if (lazerFound)
                {
                    lazer.startColor = ColorCoordinator.laserBlue(ColorCoordinator.laserTransparency);
                    lazer.endColor = ColorCoordinator.laserBlue(ColorCoordinator.alphaEmpty);
                }
                //isHittingCurvedMenu = false;
                isHittingFloatingQuad = false;
                isHittingFloatingMenu = false;
                return;
            }
        } else
        {
            if (Physics.Raycast(transform.position, forward, out hitObject))
            {
                // Do Something

                // Then Return;
                return;
            }
        }
    }

    // -------------------------------------------------------------
    // RaycastHandler Get / Set Functions
    // -------------------------------------------------------------

    // Gets the bool value of isHittingCurvedMenu
    //public bool IsHittingCurvedMenu() { return isHittingCurvedMenu; }

    // Gets the bool value of isHittingFloatingQuad
    public bool IsHittingFloatingQuad() { return isHittingFloatingQuad; }

    // Gets the bool value of isHittingFloatingMenu
    public bool IsHittingFloatingMenu() { return isHittingFloatingMenu; }

    // Gets the Hit Object and returns it
    public RaycastHit GetHitObject() { return hitObject; }


}
