﻿using UnityEngine;

public class OculusRiftControllerManager : MonoBehaviour
{
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ OculusRiftControllerManager +++ ---- ++++
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // -------------------------------------------------------------
    // OculusRiftControllerManager Variables
    // -------------------------------------------------------------
    
    private VideoPlayerHandler videoPlayerHandler;
    
    //The gamepad button that will reset VR input tracking.
    public OVRInput.RawButton resetButton = OVRInput.RawButton.Y;

    private bool
        rightAxisPressed = false,
        leftAxisPressed = false,
        // primaryIndexTriggerPressed = false,
        primaryTouchpadButtonPressed = false;

    private float
        leftAxisPressedTime = 0,
        rightAxisPressedTime = 0;

    public float
        primaryTriggerAxisHoldTime = 0.5f;

    // -------------------------------------------------------------
    // OculusRiftControllerManager Core Functions
    // -------------------------------------------------------------

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // NOTE: some of the buttons defined in OVRInput.RawButton are not available on the Android game pad controller
        if (OVRInput.GetDown(resetButton))
        {
            //*************************
            // reset orientation
            //*************************
            OVRManager.display.RecenterPose();
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            Debug.Log("backButton pressed");
        }

        // TEMPORARILY REMOVED.
        //var primaryIndexTriggerAxis = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger);

        //// If the trigger was not already being pressed..
        //if (!primaryIndexTriggerPressed)
        //{
        //    // If trigger is pressed..
        //    if (primaryIndexTriggerAxis > .5)
        //    {
        //        if (menuManager.remoteMenu.GetComponent<Canvas>().enabled)
        //        {
        //            Debug.Log("trigger pressed. init floating quad if menu open");
        //            menuManager.activateButtonMethod();
        //        }
        //        else
        //        {
        //            Debug.Log("trigger pressed but menu is closed");
        //        }
        //        primaryIndexTriggerPressed = true;
        //    }
        //}

        //// Reset primaryIndexTriggerPressed 
        //if (primaryIndexTriggerAxis == 0)
        //{
        //    primaryIndexTriggerPressed = false;
        //}

        var ovrButtonDown = OVRInput.GetDown(OVRInput.Button.One);
        var ovrButtonUp = OVRInput.GetUp(OVRInput.Button.One);

        // Does same function as the trigger.
        // Selects a item from the menu.
        // However doesn't grab the item
        if (ovrButtonDown && !primaryTouchpadButtonPressed)
        {
            primaryTouchpadButtonPressed = true;
        }

        if (ovrButtonUp)
        {
            primaryTouchpadButtonPressed = false;
        }

        var xAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).x;
        var yAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).y;

        // Listen for primarythumbstick left axis : if not already pressed
        if (!leftAxisPressed)
        {
            if (xAxis < -.8)
            {
                // Track that left axis was pressed 
                leftAxisPressed = true;
                // Track the time left axis was pressed
                leftAxisPressedTime = Time.time;

                Debug.Log(Time.time);
                Debug.Log("leftAxisPressed");
            }
        }
        else
        {
            // leftAxisPressed already pressed, check if 1 second has passed 
            if (Time.time > (leftAxisPressedTime + primaryTriggerAxisHoldTime))
            {

                // Track that left axis was pressed 
                leftAxisPressed = true;
                // Track the time left axis was pressed
                leftAxisPressedTime = Time.time;

                Debug.Log("Trigger leftAxisPressedAgain");
            }
        }

        // Listen for primarythumbstick right axis : if not already pressed
        if (!rightAxisPressed)
        {
            if (xAxis > .8)
            {
                // Track that right axis was pressed
                rightAxisPressed = true;
                // Track the time that right axis was pressed 
                rightAxisPressedTime = Time.time;

                Debug.Log("rightAxisPressed");
            }
        }
        else
        {
            // rightAxisPressed already pressed, check if 1 second has passed 
            if (Time.time > (rightAxisPressedTime + primaryTriggerAxisHoldTime))
            {

                // Track that left axis was pressed 
                rightAxisPressed = true;
                // Track the time left axis was pressed
                rightAxisPressedTime = Time.time;

                Debug.Log("Trigger rightAxisPressedAgain");
            }
        }

        // listen for 0 
        if (xAxis == 0)
        {
            leftAxisPressed = false;
            rightAxisPressed = false;
            // Debug.Log("xAxis reset");
        }
    }

    // -------------------------------------------------------------
    // OculusRiftControllerManager Get / Set Functions
    // -------------------------------------------------------------

    private GameObject GetSpawnAtRuntimeContainer() { return GameObject.Find("/SpawnAtRuntime"); }

    // -------------------------------------------------------------
    // ??? Useless Functions ???
    // -------------------------------------------------------------

    // Reset that left/right was pressed 

    //  Axis2D.PrimaryThumbstick Horizontal Movement - 1   –1.0 to 1.0 Move Left Stick
    //  Axis2D.PrimaryThumbstick Vertical Movement - 2   –1.0 to 1.0 Move Left Stick
    //Axis2D.SecondaryThumbstick Horizontal Movement - 4   –1.0 to 1.0 Move Right Stick
    //Axis2D.SecondaryThumbstick Vertical Movement - 5   –1.0 to 1.0 Move Right Stick

    //if (input.getbuttondown("fire1"))
    //{
    //    debug.log(input.mouseposition);
    //}

}
