﻿using UnityEngine;
using UnityEngine.Video;
using RenderHeads.Media.AVProVideo;

// Requires VideoPlayerHandler & DeviceManager in order to work so this just makes sure its on the same Object if added.
[RequireComponent(typeof(SpawnAtRuntime))]
public class VideoPlayerHandler: MonoBehaviour
{
    // +++++++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ VideoPlayerHandler +++ ---- ++++
    // +++++++++++++++++++++++++++++++++++++++++++++++

    /* 
     * NOTES: 
     * The folowing video formats work on Android: MP4, OGV, VP8, WEBM
     * 
     * Need to swap how I am returning variables for positions to vector3.
     * 
     */

    // -------------------------------------------------------------
    // VideoPlayerHandler Variables
    // -------------------------------------------------------------

    // Reference to video playing in sphere 
    private MediaPlayer avProMediaPlayer;

    // Reference to the SpawnAtRuntime script
    private SpawnAtRuntime spawnAtRuntime;

    // Used when checking if video has been paused after initializing
    private bool videosPaused = false;

    // References Spawners Default X,Y,Z Positions.
    public Vector3 defaultVideoPos = new Vector3(0.0f, 0.0f, 0.0f);

    // Referneces Spawners Default X,Y,Z Position Offsets;
    private Vector3 offsetVideoOne = new Vector3(0.95f, -0.8f, 0.8f);
    private Vector3 offsetVideoTwo = new Vector3(0.95f, 0.8f, 0.8f);
    private Vector3 offsetVideoThree = new Vector3(1.2f, 0.3f, 1.0f);
    private Vector3 offsetVideoFour = new Vector3(1.2f, -0.3f, 1.0f);

    // Floating Quad Video Names
    public string
        floatingVideoName1,
        floatingVideoName2,
        floatingVideoName3,
        floatingVideoName4;

    // Global Variants
    private GameObject
        floatingVideoQuad1,
        floatingVideoQuad2,
        floatingVideoQuad3,
        floatingVideoQuad4;

    // Floating Quad Video Prefabs
    public GameObject
        floatingQuadPrefab1,
        floatingQuadPrefab2,
        floatingQuadPrefab3,
        floatingQuadPrefab4;

    // Reference to avProVideo MediaPlayer
    private float
        currentAVProTime,
        mediaPlayerTime = 0.0f;

    // -------------------------------------------------------------
    // VideoPlayerHandler Core Functions
    // -------------------------------------------------------------

    private void Awake()
    {
        // Get the SpawnAtRuntime script from the current GameObject
        spawnAtRuntime = gameObject.GetComponent<SpawnAtRuntime>();

        // Get the AVProMediaPlayer
        avProMediaPlayer = spawnAtRuntime.GetMediaPlayer();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Set currentAVProTime to the current AVProVideo MediaPlayer Time
        currentAVProTime = avProMediaPlayer.Control.GetCurrentTimeMs();

        // If the videosPaused does not equal true then pause all the floating video's
        if (!videosPaused)
        {
            //Debug.Log("media plyaer: ");
            // Pause them 
            GetFloatingQuadVideoPlayer(1).Pause();
            GetFloatingQuadVideoPlayer(2).Pause();
            GetFloatingQuadVideoPlayer(3).Pause();
            GetFloatingQuadVideoPlayer(4).Pause();

            // Set videosPaused to true
            videosPaused = true;
        }
    }

    // -------------------------------------------------------------
    // VideoPlayerHandler Get / Set Functions
    // -------------------------------------------------------------

    // Get the current AVProMediaplayer time
    public float GetCurrentAVProTime() { return currentAVProTime; }

    // Get the default X, Y, & Z values for the floating quads
    public Vector3 GetDefaultPosInSpace() { return defaultVideoPos; }

    // Get the default X, Y, & Z offset values for the floating quads
    public Vector3 GetFloatingVideoOffset(int videoObject)
    {
        if (videoObject == 1) { return offsetVideoOne; }
        else if (videoObject == 2) { return offsetVideoTwo; }
        else if (videoObject == 3) { return offsetVideoThree; }
        else if (videoObject == 4) { return offsetVideoFour; }
        else { return new Vector3(0,0,0); }

    }

    // Passing the floating gameobject prefab - will be changed later for dynamic use
    public GameObject GetFloatingVideoPlayerPrefab(int objectNumber)
    {
        switch (objectNumber)
        {
            case 1:
                return floatingQuadPrefab1;
            case 2:
                return floatingQuadPrefab2;
            case 3:
                return floatingQuadPrefab3;
            case 4:
                return floatingQuadPrefab4;
            default:
                return null;
        }
    }

    // Passing the floating gameobject - will be changed later for dynamic use
    public GameObject GetFloatingVideoPlayerObj(int objectNumber)
    {
        switch (objectNumber)
        {
            case 1:
                return floatingVideoQuad1;
            case 2:
                return floatingVideoQuad2;
            case 3:
                return floatingVideoQuad3;
            case 4:
                return floatingVideoQuad4;
            default:
                return null;
        }
    }

    // Passing the floating gameobject - will be changed later for dynamic use
    public void SetFloatingVideoPlayerObj(int objectNumber, GameObject which)
    {
        switch (objectNumber)
        {
            case 1:
                floatingVideoQuad1 = which;
                break;
            case 2:
                floatingVideoQuad2 = which;
                break;
            case 3:
                floatingVideoQuad3 = which;
                break;
            case 4:
                floatingVideoQuad4 = which;
                break;
        }
    }

    // Passing the floating gameobject videoplayer - will be changed later for dynamic use
    public VideoPlayer GetFloatingQuadVideoPlayer(int objectNumber)
    {
        switch (objectNumber)
        {
            case 1:
                return GetFloatingVideoPlayerObj(1).GetComponent<VideoPlayer>();
            case 2:
                return GetFloatingVideoPlayerObj(2).GetComponent<VideoPlayer>();
            case 3:
                return GetFloatingVideoPlayerObj(3).GetComponent<VideoPlayer>();
            case 4:
                return GetFloatingVideoPlayerObj(4).GetComponent<VideoPlayer>();
            default:
                return null;
        }
    }

    // Passing the floating gameobject videoplayer - will be changed later for dynamic use
    public string GetFloatingQuadVideoName(int objectNumber)
    {
        switch (objectNumber)
        {
            case 1:
                return floatingVideoName1;
            case 2:
                return floatingVideoName2;
            case 3:
                return floatingVideoName3;
            case 4:
                return floatingVideoName4;
            default:
                return null;
        }
    }

    // -------------------------------------------------------------
    // VideoPlayerHandler Sync Functions
    // -------------------------------------------------------------

    // Update a specific floating video to the main video
    public void updateVideoTime(
        MediaPlayer defaultMediaPlayer,
        VideoPlayer toBeSyncedFloatingVideoPlayer)
    {
        float currentMPT = defaultMediaPlayer.Control.GetCurrentTimeMs();
        float currentMPTinSeconds = currentMPT / 1000;
        double currentMPTconvertedToDouble = ConvertFloatToDouble(currentMPTinSeconds);

        // Syncs the button videoplayer with the main mediaplayer
        toBeSyncedFloatingVideoPlayer.time = currentMPTconvertedToDouble;
    }

    // -------------------------------------------------------------
    // Handy Functions
    // -------------------------------------------------------------

    // Converts a float var to a double var 
    private double ConvertFloatToDouble(float flt)
    {
        decimal dec = new decimal(flt);
        double db = (double)dec;
        return db;
    }

    // Notifier function that returns a bool depending on if all videoplayers are visible
    public bool AreAllVideosVisible()
    {
        if (GetFloatingVideoPlayerObj(1).gameObject.GetComponent<MeshRenderer>().enabled &&
            GetFloatingVideoPlayerObj(2).gameObject.GetComponent<MeshRenderer>().enabled &&
            GetFloatingVideoPlayerObj(3).gameObject.GetComponent<MeshRenderer>().enabled &&
            GetFloatingVideoPlayerObj(4).gameObject.GetComponent<MeshRenderer>().enabled)
        { return true; }
        else
        { return false; }
    }

    // -------------------------------------------------------------
    // ??? Useless Functions ???
    // -------------------------------------------------------------

    // Video Player on Menu Button
    //private VideoPlayer
    //    remoteButtonVideoPlayer1,
    //    remoteButtonVideoPlayer2,
    //    remoteButtonVideoPlayer3,
    //    remoteButtonVideoPlayer4;

    //public GameObject instantiateVideoPlayer(
    //        GameObject floatingVideoObject,
    //        float defaultFloatingVideoPositionX,
    //        float defaultFloatingVideoPositionY,
    //        float defaultFloatingVideoPositionZ)
    //{
    //    return Instantiate(floatingVideoObject, new Vector3(defaultFloatingVideoPositionX, defaultFloatingVideoPositionY, defaultFloatingVideoPositionZ), Quaternion.identity);
    //}

    //public void instantantiedQuadFloatingVideoPlayerObject(int objectNumber)
    //{

    //    switch (objectNumber)
    //    {
    //        case (1):
    //            instantantiedQuadFloatingVideoPlayerObject1 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab1, new Vector3(0.95f + defaultVideoFloatX, -0.8f + defaultVideoFloatY, 0.8f + defaultVideoFloatZ));
    //            break;
    //        case (2):
    //            instantantiedQuadFloatingVideoPlayerObject2 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab2, new Vector3(0.95f + defaultVideoFloatX, 0.8f + defaultVideoFloatY, 0.8f + defaultVideoFloatZ));
    //            break;
    //        case (3):
    //            instantantiedQuadFloatingVideoPlayerObject3 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab3, new Vector3(1.2f + defaultVideoFloatX, 0.3f + defaultVideoFloatY, 1.0f + defaultVideoFloatZ));
    //            break;
    //        case (4):
    //            instantantiedQuadFloatingVideoPlayerObject4 = spawnAtRuntime.spawnSomething(floatingQuadVideoPlayerObjectPrefab4, new Vector3(1.2f + defaultVideoFloatX, -0.3f + defaultVideoFloatY, 1.0f + defaultVideoFloatZ));
    //            break;
    //        default:
    //            break;
    //    }

    //}
}
