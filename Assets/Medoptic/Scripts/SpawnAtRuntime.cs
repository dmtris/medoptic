﻿using System;
using System.Collections;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using UnityEngine.Video;

public class SpawnAtRuntime : MonoBehaviour
{
    // Reference to the VideoPlayerHandler script
    private VideoPlayerHandler videoPlayerHandler;

    // Reference to the VideoPlayerHandler script
    private VideoPlayerButtonHandler videoPlayerButtonHandler;

    // Reference to the MenuHandler script
    private MenuHandler
        menuHandler;

    // Used as a notifier that the remote menu has been spawned
    private bool remoteMenuExists = false;

    // Reference to the DeviceManager script
    private DeviceManager deviceManager;

    // Reference to the AVProVideoPlayer
    private MediaPlayer
        avProMediaPlayer;

    // Reference to the 360 Shpere GameObject
    public GameObject
        sphereContainer;

    // Used to track controller recenter event
    private int
        oldResetCount = 0,
        currentResetCount = 0;

    private void Awake()
    {
        // Set the current recenter event count to a int
        // It will be used later to reset the scene position later.
        setControllerRecenterCount();

        // Get the DeviceManager script attached to this object
        deviceManager = gameObject.GetComponent<DeviceManager>();

        // Get the VideoPlayerHandler script attached to this object
        videoPlayerHandler = gameObject.GetComponent<VideoPlayerHandler>();

        // Get the VideoPlayerHandler script attached to this object
        videoPlayerButtonHandler = gameObject.GetComponent<VideoPlayerButtonHandler>();
    }

    // Use this for initialization
    void Start()
    {
        // Spawn the floating quad prefabs and set the to a value.
        videoPlayerHandler.SetFloatingVideoPlayerObj(1, SpawnSomething(videoPlayerHandler.GetFloatingVideoPlayerPrefab(1),
            new Vector3(
                videoPlayerHandler.GetFloatingVideoOffset(1).x + videoPlayerHandler.GetDefaultPosInSpace().x,
                videoPlayerHandler.GetFloatingVideoOffset(1).y + videoPlayerHandler.GetDefaultPosInSpace().y,
                videoPlayerHandler.GetFloatingVideoOffset(1).z + videoPlayerHandler.GetDefaultPosInSpace().z)));
        videoPlayerHandler.SetFloatingVideoPlayerObj(2, SpawnSomething(videoPlayerHandler.GetFloatingVideoPlayerPrefab(2),
            new Vector3(
                videoPlayerHandler.GetFloatingVideoOffset(2).x + videoPlayerHandler.GetDefaultPosInSpace().x,
                videoPlayerHandler.GetFloatingVideoOffset(2).y + videoPlayerHandler.GetDefaultPosInSpace().y,
                videoPlayerHandler.GetFloatingVideoOffset(2).z + videoPlayerHandler.GetDefaultPosInSpace().z)));
        videoPlayerHandler.SetFloatingVideoPlayerObj(3, SpawnSomething(videoPlayerHandler.GetFloatingVideoPlayerPrefab(3),
            new Vector3(
                videoPlayerHandler.GetFloatingVideoOffset(3).x + videoPlayerHandler.GetDefaultPosInSpace().x,
                videoPlayerHandler.GetFloatingVideoOffset(3).y + videoPlayerHandler.GetDefaultPosInSpace().y,
                videoPlayerHandler.GetFloatingVideoOffset(3).z + videoPlayerHandler.GetDefaultPosInSpace().z)));
        videoPlayerHandler.SetFloatingVideoPlayerObj(4, SpawnSomething(videoPlayerHandler.GetFloatingVideoPlayerPrefab(4),
            new Vector3(
                videoPlayerHandler.GetFloatingVideoOffset(4).x + videoPlayerHandler.GetDefaultPosInSpace().x,
                videoPlayerHandler.GetFloatingVideoOffset(4).y + videoPlayerHandler.GetDefaultPosInSpace().y,
                videoPlayerHandler.GetFloatingVideoOffset(4).z + videoPlayerHandler.GetDefaultPosInSpace().z)));

        // Setting the Floating Object Name to the same as the Prefab
        videoPlayerHandler.GetFloatingVideoPlayerObj(1).name = videoPlayerHandler.GetFloatingVideoPlayerPrefab(1).name;
        videoPlayerHandler.GetFloatingVideoPlayerObj(2).name = videoPlayerHandler.GetFloatingVideoPlayerPrefab(2).name;
        videoPlayerHandler.GetFloatingVideoPlayerObj(3).name = videoPlayerHandler.GetFloatingVideoPlayerPrefab(3).name;
        videoPlayerHandler.GetFloatingVideoPlayerObj(4).name = videoPlayerHandler.GetFloatingVideoPlayerPrefab(4).name;

        // Setting the Floating VideoPlayer Video Name via String
        videoPlayerHandler.GetFloatingVideoPlayerObj(1).GetComponent<CastVideoToShape>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(1));
        videoPlayerHandler.GetFloatingVideoPlayerObj(2).GetComponent<CastVideoToShape>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(2));
        videoPlayerHandler.GetFloatingVideoPlayerObj(3).GetComponent<CastVideoToShape>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(3));
        videoPlayerHandler.GetFloatingVideoPlayerObj(4).GetComponent<CastVideoToShape>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(4));

        //// Setting the minDepthDistance & the maxDepthDistance of the StandardGrabReceiver script programically
        //videoPlayerHandler.GetFloatingVideoPlayerObj(1).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().minDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(1).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(1).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().maxDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(1).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(2).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().minDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(2).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(2).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().maxDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(2).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(3).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().minDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(3).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(3).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().maxDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(3).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(4).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().minDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(4).transform.position.z;
        //videoPlayerHandler.GetFloatingVideoPlayerObj(4).GetComponent<EasyInputVR.StandardControllers.StandardGrabReceiver>().maxDepthDistance = videoPlayerHandler.GetFloatingVideoPlayerObj(4).transform.position.z;

        // Check to see if a controller was set originally
        if (deviceManager.GetControllerGameObject() != null)
        {
            // If found then Spawn the Controller Video Menu
            videoPlayerButtonHandler.SetVideoPlayerButtonMenuObj(SpawnSomething(videoPlayerButtonHandler.GetVideoPlayerButtonMenuPrefab()));

            // Setting the Controller Video Menu name to the same as the prefab
            videoPlayerButtonHandler.GetVideoPlayerButtonMenuObj().name = videoPlayerButtonHandler.GetVideoPlayerButtonMenuPrefab().name;

            // Setting Controller Video Menu as child of the Headset Controller
            SetObjAsChildofObj(videoPlayerButtonHandler.GetVideoPlayerButtonMenuObj(), deviceManager.GetControllerGameObject());

            // Set the Video Names to the Buttons associated with the Prefab
            videoPlayerButtonHandler.GetVideoPlayerButtonMenuObj().transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<CastVideoToRawImage>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(1));
            videoPlayerButtonHandler.GetVideoPlayerButtonMenuObj().transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<CastVideoToRawImage>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(2));
            videoPlayerButtonHandler.GetVideoPlayerButtonMenuObj().transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<CastVideoToRawImage>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(3));
            videoPlayerButtonHandler.GetVideoPlayerButtonMenuObj().transform.GetChild(0).GetChild(3).GetChild(0).GetComponent<CastVideoToRawImage>().SetVideoAssetName(videoPlayerHandler.GetFloatingQuadVideoName(4));

            // Used as a notifier that the remote menu has been spawned
            remoteMenuExists = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Check the current recenter event count against the old
        checkControllerRecenterCount(hasRecenterCountChanged());
    }

    // -------------------------------------------------------------
    // SpawnAtRuntime Get / Set Functions
    // -------------------------------------------------------------

    // Get the 360 Shpere GameObject
    public GameObject GetSphereContainer() { return sphereContainer; }

    // Get the 360 Sphere MediaPlayer
    public MediaPlayer GetMediaPlayer() { return GetSphereContainer().transform.GetChild(1).GetComponent<MediaPlayer>(); }

    // Get the notifier for the remote menu
    public bool GetRemoteMenuNotifierValue() { return remoteMenuExists; }

    // -------------------------------------------------------------
    // Handy Functions
    // -------------------------------------------------------------

    // Spawn Something
    public GameObject SpawnSomething(GameObject what, Vector3 vec) { return Instantiate(what, vec, Quaternion.identity); }
    public GameObject SpawnSomething(GameObject what) { return Instantiate(what, what.transform.position, what.transform.rotation); }

    // Set Something as a Child of a GameObject
    public void SetObjAsChildofObj(GameObject what, GameObject parentObj) { what.transform.parent = parentObj.transform; }

    // Used to intercept recenter functionality and use it to recenter the headset.
    private void setControllerRecenterCount() { oldResetCount = OVRInput.GetControllerRecenterCount(); }

    public bool hasRecenterCountChanged()
    {
        if (OVRInput.GetControllerRecenterCount() != oldResetCount) { return true; }
        else { return false;}
    }

    public void checkControllerRecenterCount(bool hasRecenterCountChanged)
    {
        if (hasRecenterCountChanged)
        {
            // This is how you reset the headset pos
            OVRManager.display.RecenterPose();
            oldResetCount = OVRInput.GetControllerRecenterCount();
        }
    }

    // Reset VideoPlayer Object to default position
    public void ResetFloatingQuadVideoPlayerObjPos(int obj)
    {
        try
        {
            // Move Floating VideoPlayer Object to default position
            videoPlayerHandler.GetFloatingVideoPlayerObj(obj).transform.position = new Vector3(
                videoPlayerHandler.GetFloatingVideoOffset(obj).x + videoPlayerHandler.GetDefaultPosInSpace().x,
                videoPlayerHandler.GetFloatingVideoOffset(obj).y + videoPlayerHandler.GetDefaultPosInSpace().y,
                videoPlayerHandler.GetFloatingVideoOffset(obj).z + videoPlayerHandler.GetDefaultPosInSpace().z);
        }
        catch(System.Exception ex)
        { Debug.Log("++++ RESET FloatingQuadVideoPlayer Position ERROR: ++++ " + ex); }
    }

    // This needs to be made dynamic down the road
    // Updates all the button videoplayer times and floating quad videoplayer times.
    public void updateAllVideoTimes(MediaPlayer mediaPlayer)
    {
        videoPlayerHandler.updateVideoTime(mediaPlayer, videoPlayerHandler.GetFloatingQuadVideoPlayer(1));
        if(deviceManager.GetControllerGameObject() != null)
        {
            videoPlayerButtonHandler.updateVideoTime(mediaPlayer, videoPlayerButtonHandler.GetButtonVideoPlayer(1));
        }

        //videoPlayerHandler.updateVideoTime(mediaPlayer, videoPlayerHandler.GetFloatingQuadVideoPlayer(2));
        //videoPlayerButtonHandler.updateVideoTime(mediaPlayer, videoPlayerButtonHandler.GetButtonVideoPlayer(2));

        //videoPlayerHandler.updateVideoTime(mediaPlayer, videoPlayerHandler.GetFloatingQuadVideoPlayer(3));
        //videoPlayerButtonHandler.updateVideoTime(mediaPlayer, videoPlayerButtonHandler.GetButtonVideoPlayer(3));

        //videoPlayerHandler.updateVideoTime(mediaPlayer, videoPlayerHandler.GetFloatingQuadVideoPlayer(4));
        //videoPlayerButtonHandler.updateVideoTime(mediaPlayer, videoPlayerButtonHandler.GetButtonVideoPlayer(4));
    }
}
