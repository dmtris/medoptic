﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSFM : MonoBehaviour {

    public GameObject master;
    public GameObject slave;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        slave.transform.position = master.transform.position;
        slave.transform.rotation = master.transform.rotation;
    }
}
