﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetDeviceName : MonoBehaviour {
    
    private TextMesh textMesh;

	// Use this for initialization
	void Start () {
        textMesh = this.GetComponent<TextMesh>();
        textMesh.text =
            "Device Model: " + SystemInfo.deviceModel + "\n" + 
            "Device Name: " + SystemInfo.deviceName + "\n";
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
