﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugPositionAndRotation : MonoBehaviour {

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ Debug Position & Rotation +++ ---- ++++
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++

    /*
     * NOTES:
     * Useful CMD command to check debug of Android Headsets on Windows:
     * adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG
     * 
     */

    // -------------------------------------------------------------
    // Debug Position & Rotation Variables
    // -------------------------------------------------------------

    [SerializeField]
    private int scriptDelay = 10;
    [SerializeField]
    private Vector3 position;
    [SerializeField]
    private Quaternion rotation;

    // -------------------------------------------------------------
    // Debug Position & Rotation Core Functions
    // -------------------------------------------------------------

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        position = gameObject.transform.position;
        rotation = gameObject.transform.rotation;
        StartCoroutine(RunDebugScript());
    }

    public IEnumerator RunDebugScript()
    {
        WaitForSeconds waitTime = new WaitForSeconds(scriptDelay);

        yield return waitTime;

        Debug.Log(
            "================================================================" + " \n" +
            " +++ DEBUGGING " + gameObject.name + " +++ " + " \n" +
            "================================================================" + " \n" +
            " POSITION: " + position + " \n" +
            " ROTATION: " + rotation + " \n"+
            "================================================================");
    }
}
