﻿using UnityEngine;

public class MaterialTransparency : MonoBehaviour {

    public float blackness = 0.5f; 
    public float alpha = 1.0f; 
    float previousAlpha;
    float previousBlackness; 

    Material theMaterial; 

	// Use this for initialization
	void Start () {

        theMaterial = gameObject.GetComponent<MeshRenderer>().material;
        SetAlpha(alpha);
        SetBlackness(blackness); 
    }

    // Update is called once per frame
    void Update () {
        if (previousAlpha != alpha)
        {
            SetAlpha(alpha);
        }

        if (previousBlackness != blackness)
        {
            SetBlackness(blackness); 
        }
    }

    void SetAlpha(float alpha)
    {
        // Here you assign a color to the referenced material,
        // changing the color of your renderer
        theMaterial.SetFloat("_Alpha", alpha);
        previousAlpha = alpha;
    }


    void SetBlackness(float blackness)
    { 
        // set blackness variable for shader
        theMaterial.SetFloat("_Black", blackness); 
        previousBlackness = blackness;
    }
}
