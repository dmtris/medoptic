﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FollowTarget : MonoBehaviour {

    // ++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ FollowTarget +++ ---- ++++
    // ++++++++++++++++++++++++++++++++++++++++

    // -------------------------------------------------------------
    // FollowTarget Variables
    // -------------------------------------------------------------
    
    public bool
        affectADifferentObj = false;

    public GameObject
        parentObject,
        affectedObject;
    
    public bool
        matchExactPosition,
        matchPositionChange,
        matchExactRotation,
        matchRotationChange;

    private Vector3
        previousParentPosition,
        currentParentPosition,
        previousAffectedPosition,
        positionDiff;

    private Quaternion
        previousParentRotation,
        currentParentRotation,
        previousAffectedRotation,
        rotationDiff;

    // -------------------------------------------------------------
    // FollowTarget Core Functions
    // -------------------------------------------------------------

    // Use this for initialization
    void Start()
    {
        if (!affectADifferentObj) { affectedObject = this.gameObject; }
    }

    // Update is called every fixed framerate frame
    void FixedUpdate()
    {
        if(matchExactPosition)
        {
            affectedObject.transform.position = parentObject.transform.position;
        }
        else if (matchPositionChange)
        {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                previousParentPosition = parentObject.transform.position;
                previousAffectedPosition = affectedObject.transform.position;
            }
            if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
            {
                currentParentPosition = parentObject.transform.position;
                positionDiff = (previousParentPosition - currentParentPosition);
                affectedObject.transform.position = (previousAffectedPosition + positionDiff);
            }
        }
        else if (matchExactRotation)
        {
            affectedObject.transform.rotation = parentObject.transform.rotation;
        }
        else if (matchRotationChange)
        {
            // This may not work will need to get a professional to look at this.
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                previousParentRotation = parentObject.transform.rotation;
                previousAffectedRotation = affectedObject.transform.rotation;
            }
            if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
            {
                currentParentRotation = parentObject.transform.rotation;
                rotationDiff = (Quaternion.Inverse(previousParentRotation) * currentParentRotation);
                affectedObject.transform.rotation = (previousAffectedRotation * rotationDiff);
            }
        }
    }
}

#if UNITY_EDITOR
// -------------------------------------------------------------
// FollowTarget Inspecttor Functions
// -------------------------------------------------------------
[CustomEditor(typeof(FollowTarget))]
    public class FollowTargetEditor : Editor
{

    int selected = 0;

    string[] options = new string[]
    {
        "Match Exact Position",
        "Match Position Change",
        "Match Exact Rotation",
        "Match Rotation Change"
    };

    public override void OnInspectorGUI ()
    {
        var followTarget = target as FollowTarget;

        EditorGUILayout.LabelField("Object To Be Followed");

        EditorGUILayout.Space();

        EditorGUI.indentLevel++;

        followTarget.affectADifferentObj = (bool)EditorGUILayout.Toggle(new GUIContent("Affect A Different Obj", "If set to true then you can set a different object to be affected by the FollowTarget script."), followTarget.affectADifferentObj);

        if (followTarget.affectADifferentObj)
        {
            followTarget.affectedObject = (GameObject)EditorGUILayout.ObjectField(new GUIContent("Affected Object", "The Object to be affected by the Target Object."), followTarget.affectedObject, typeof(GameObject), true);
        }

        followTarget.parentObject = (GameObject)EditorGUILayout.ObjectField(new GUIContent("Parent Object", "The parent Object to be followed."), followTarget.parentObject, typeof(GameObject), true);

        EditorGUI.indentLevel--;

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Object Follow Behavior");

        EditorGUILayout.Space();

        EditorGUI.indentLevel++;

        selected = EditorGUILayout.Popup(new GUIContent("Object Follow Behavior"), selected, options);

        switch(selected)
        {
            case 0:
                EditorGUILayout.HelpBox(new GUIContent("Use the same position as the object you are following."));
                followTarget.matchExactPosition = true;
                followTarget.matchPositionChange = false;
                followTarget.matchExactRotation = false;
                followTarget.matchRotationChange = false;
                break;

            case 1:
                EditorGUILayout.HelpBox(new GUIContent("Use the difference between the targets last position values and it's new position values to imitate its positional change."));
                followTarget.matchExactPosition = false;
                followTarget.matchPositionChange = true;
                followTarget.matchExactRotation = false;
                followTarget.matchRotationChange = false;
                break;

            case 2:
                EditorGUILayout.HelpBox(new GUIContent("Use the same rotation as the object you are following."));
                followTarget.matchExactPosition = false;
                followTarget.matchPositionChange = false;
                followTarget.matchExactRotation = true;
                followTarget.matchRotationChange = false;
                break;

            case 3:
                EditorGUILayout.HelpBox(new GUIContent("Use the difference between the targets last rotation values and it's current rotation values to imitate its rotational change."));
                followTarget.matchExactPosition = false;
                followTarget.matchPositionChange = false;
                followTarget.matchExactRotation = false;
                followTarget.matchRotationChange = true;
                break;
        }

        EditorGUI.indentLevel--;
    }
}
#endif
