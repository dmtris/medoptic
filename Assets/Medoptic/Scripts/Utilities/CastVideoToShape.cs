﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]

public class CastVideoToShape : MonoBehaviour
{
    private VideoPlayer video;
    private MeshRenderer render;

    private bool videoNameExists = false;

    private string
        videoAssetName = null,
        regDataPath,
        jarDataPath;

    private void Awake()
    {
        video = GetComponent<VideoPlayer>();
        render = GetComponent<MeshRenderer>();

        regDataPath = Application.dataPath + "/StreamingAssets/";
        jarDataPath = "jar:file://" + Application.dataPath + "!/assets/";
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!videoNameExists && videoAssetName != null)
        {
#if UNITY_EDITOR
            // If unity editor then use this file path to point to video player assets.
            //video.url = riftVideoLocation + videoAssetName + ".mp4";
            
            video.url = regDataPath + videoAssetName;

#elif !UNITY_EDITOR && UNITY_ANDROID
            // Else if it not unity and it is being built for android then point video player assets to the jar location.
            video.url = jarDataPath + videoAssetName;
            // video.url = "file://" + goVideoLocation + videoAssetName;
#endif
            StartCoroutine(PlayVideo());
            videoNameExists = true;
        }

    }

    // Set the name of the Video Asset
    public void SetVideoAssetName(string name) { videoAssetName = name; }

    // Play the Video once Loaded
    public IEnumerator PlayVideo()
    {
        video.Prepare();

        WaitForSeconds waitTime = new WaitForSeconds(5);
        while (!video.isPrepared)
        {
            Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        }

        render.enabled = true;
        video.Play();

        GetSpawnAtRuntimeContainer().GetComponent<MenuHandler>().SyncAllVideoTimes();

    }

    public GameObject GetSpawnAtRuntimeContainer() { return GameObject.Find("/SpawnAtRuntime"); }
}
