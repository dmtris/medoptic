﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent
    (typeof(RawImage),
    typeof(VideoPlayer))]

public class CastVideoToRawImage : MonoBehaviour
{
    private VideoPlayer video;
    private RawImage rawImage;

    private string videoAssetName = null;
    private bool videoAssetNameExists = false;

    // private string riftVideoLocation;
    // private string goVideoLocation;

    private void Awake()
    {
        // riftVideoLocation = "D:/Unity-Projects/Medoptic-Videos/";
        // goVideoLocation = Application.persistentDataPath + "/Videos/";
    }

    // Use this for initialization
    void Start()
    {
        rawImage = GetComponent<RawImage>();
        video = GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(videoAssetName != null && !videoAssetNameExists)
        {
#if UNITY_EDITOR
            // If unity editor then use this file path to point to video player assets.
            //video.url = riftVideoLocation + videoAssetName + ".mp4";
            video.url = Application.dataPath + "/StreamingAssets/" + videoAssetName;

#elif !UNITY_EDITOR && UNITY_ANDROID
            // Else if it not unity and it is being built for android then point video player assets to the jar location.
            video.url = "jar:file://" + Application.dataPath + "!/assets/" + videoAssetName;
            // video.url = "file://" + goVideoLocation + videoAssetName;
#endif
            StartCoroutine(PlayVideo());

            // The Name of the Video was found so set this to true
            videoAssetNameExists = true;
        }

        if (rawImage.texture == null)
        {
            rawImage.texture = video.texture;
        } 
    }

    // Set the name of the Video Asset
    public void SetVideoAssetName(string name) { videoAssetName = name; }

    // Play the Video once Loaded
    IEnumerator PlayVideo()
    { 
        video.Prepare(); 

        //Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!video.isPrepared)
        {
            Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        } 
        rawImage.texture = video.texture; 
        video.Play(); 
    } 
}
