﻿using UnityEngine;

public class ColorCoordinator : MonoBehaviour {

    // ++++++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ ColorCoordinator +++ ---- ++++
    // ++++++++++++++++++++++++++++++++++++++++++++

    /*
     * NOTES:
     * Handles the various Colors that we use for MedOptic
     * 
     * #2bb2fd #3bf dodgerbluehsl(201,98,58) rgb(43,178,253)
     * #1a1a1a #222 blackhsl(0,0,10) rgb(26,26,26)
     * #303030 #333 darkslategrayhsl(0,0,18) rgb(48,48,48)
     * #46d476 #4d7 mediumseagreenhsl(140,62,55)rgb(70,212,118)
     */

    // -------------------------------------------------------------
    // ColorCoordinator Variables
    // -------------------------------------------------------------

    public static float alphaSolid = 1.0f;
    public static float alphaEmpty = 0.0f;
    public static float laserTransparency = 0.3f;

    // -------------------------------------------------------------
    // ColorCoordinator Core Functions
    // -------------------------------------------------------------

    public static Color laserBlue(float alpha)
    {
        Color laserBlue = new Color();
        ColorUtility.TryParseHtmlString("#80FFFF", out laserBlue);
        laserBlue.a = alpha;
        return laserBlue;
    }

    public static Color dodgerBlueHSL(float alpha)
    {
        Color laserBlue = new Color();
        ColorUtility.TryParseHtmlString("#2bb2fd", out laserBlue);
        laserBlue.a = alpha;
        return laserBlue;
    }

    public static Color blackHSL(float alpha)
    {
        Color laserBlue = new Color();
        ColorUtility.TryParseHtmlString("#1a1a1a", out laserBlue);
        laserBlue.a = alpha;
        return laserBlue;
    }

    public static Color darkSlateGrayHSL(float alpha)
    {
        Color laserBlue = new Color();
        ColorUtility.TryParseHtmlString("#303030", out laserBlue);
        laserBlue.a = alpha;
        return laserBlue;
    }

    public static Color mediumSeaGreenHSL(float alpha)
    {
        Color laserBlue = new Color();
        ColorUtility.TryParseHtmlString("#46d476", out laserBlue);
        laserBlue.a = alpha;
        return laserBlue;
    }

}
