﻿using UnityEngine;

public class LookAtTarget : MonoBehaviour {
    
    private GameObject thisObject;
    private GameObject currentCameraRigContainer;
    private DeviceManager deviceManager;
    private GameObject cameraObj;

    // Use this for initialization
    void Start () {
        thisObject = this.gameObject;
        deviceManager = GetSpawnAtRuntime().GetComponent<DeviceManager>();
        cameraObj = deviceManager.GetCurrentCameraRigContainer();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        // Vector3 lookPoint = thisObject.transform.position - target.transform.position;
        // lookPoint.y = target.transform.position.y;

        // thisObject.transform.LookAt(target.transform.position);

        // transform.rotation = Quaternion.LookRotation(transform.position - target.transform.position);
        if(cameraObj != null)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - cameraObj.transform.position);
        } else
        {
            transform.rotation = Quaternion.LookRotation(transform.position);
        }
    }
    private GameObject GetSpawnAtRuntime() { return GameObject.Find("/SpawnAtRuntime"); }

}
