﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class AutoSetCanvasBoxColliderSize : MonoBehaviour {
    
    private GameObject thisObj;
    private BoxCollider thisObjCollider;
    private RectTransform thisObjCanvasRectTransform;
    private Vector2 vec2;


    // Use this for initialization
    void Start () {
        thisObj = this.gameObject;
        thisObjCollider = thisObj.GetComponent<BoxCollider>();
        thisObjCanvasRectTransform = thisObj.GetComponent<RectTransform>();

    }
	
	// Update is called once per frame
	void Update () {
        vec2 = thisObjCanvasRectTransform.sizeDelta;
        thisObjCollider.center = new Vector3(0, -(vec2.y / 2), 0);
        thisObjCollider.size = new Vector3(vec2.x, vec2.y, 0);
	}
}
