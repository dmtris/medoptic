﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class DeviceManager : MonoBehaviour
{
    // +++++++++++++++++++++++++++++++++++++++++
    // ++++ ---- +++ DeviceManager +++ ---- ++++
    // +++++++++++++++++++++++++++++++++++++++++

    /* 
     * NOTES: 
     * The OVRCameraRig needs to be set to active or the project will error out.
     * If not, you will recieve a "object reference not set to an instance of an object" error.
     * 
     */

    // -------------------------------------------------------------
    // DeviceManager Variables
    // -------------------------------------------------------------

    public GameObject oculusContainer;
    public GameObject sphereVideoObj;
    public GameObject floatingOptionsMenu;

    // -------------------------------------------------------------
    // DeviceManager Core Functions
    // -------------------------------------------------------------

    private void Awake()
    {
        // Set the CameraRig to video sphere in 360SphereVideo
        SetSphereCameraRig();
    }

    // -------------------------------------------------------------
    // DeviceManager Get / Set Functions
    // -------------------------------------------------------------

    // Get device model as a String
    public string GetDeviceModel() { return SystemInfo.deviceModel; }

    // Return the OVRCameraRig gameobject
    public GameObject GetOculusContainer() { if(oculusContainer != null) { return oculusContainer; } else { if (GameObject.Find("/OVRCameraRig")) { return GameObject.Find("/OVRCameraRig"); } else { return null; } } }

    // Return the GameObject of video sphere in 360SphereVideo
    public GameObject GetSphereVideoObj() { if (sphereVideoObj != null) { return sphereVideoObj.transform.GetChild(0).gameObject; } else { if (GameObject.Find("/360SphereVideo")) { return GameObject.Find("/360SphereVideo").transform.GetChild(0).gameObject; } else  { return null; } } }

    // Set the GameObject of video sphere in 360SphereVideo
    public void SetSphereVideoObjCameraRig(Camera cameraRig) { if (GetSphereVideoObj() != null) { GetSphereVideoObj().GetComponent<UpdateStereoMaterial>()._camera = cameraRig; } }

    // Enable the OculusCameraRig and return its main camera
    public Camera GetOculusCameraRig() { return GetOculusContainer().transform.GetChild(0).GetChild(1).GetComponent<Camera>(); }

    // Get Oculus Rift Controller
    // private GameObject GetOculusRiftControllerContainer() { return GetOculusContainer().transform.GetChild(0).Find("RightHandAnchor").gameObject; }
    private GameObject GetOculusRiftControllerContainer() { return GetOculusContainer().transform.GetChild(0).GetChild(5).gameObject; }

    // Get Oculus Rift Controller
    // private GameObject GetOculusGoControllerContainer() { return GetOculusContainer().transform.GetChild(0).Find("TrackedRemote").gameObject; }
    private GameObject GetOculusGoControllerContainer() { return GetOculusContainer().transform.GetChild(0).GetChild(6).gameObject; }

    // Return the UI Gameobject Canvas (Vertical Layout) in the FloatingOptionsMenu
    public GameObject GetFloatingOptionsMenuObj() { if (floatingOptionsMenu != null) { return floatingOptionsMenu.transform.GetChild(1).GetChild(0).GetChild(0).gameObject; } else { if (GameObject.Find("/FloatingOptionsMenu") != null) { return GameObject.Find("/FloatingOptionsMenu").transform.GetChild(1).GetChild(0).GetChild(0).gameObject; } else { return null; } } }

    // Return one of the Child UI GameObjects inside of the Canvas (Vertical Layout) in the FloatingOptionsMenu
    public GameObject GetFloatingOptionsMenuChildObj(int objPos)
    {
        switch (objPos)
        {
            case 0:
                // MainMenu -> MenuObjs -> 3DVideoObj
                return GetFloatingOptionsMenuObj().transform.GetChild(0).GetChild(1).GetChild(0).gameObject;
            case 1:
                // MainMenu -> MenuObjs -> ShowMIOs
                return GetFloatingOptionsMenuObj().transform.GetChild(0).GetChild(1).GetChild(1).gameObject;
            case 2:
                // AudioOptions -> MenuObjs -> VolumeSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(1).GetChild(1).GetChild(0).GetChild(1).gameObject;
            case 3:
                // AudioOptions -> MenuObjs -> BalanceSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(1).GetChild(1).GetChild(1).GetChild(1).gameObject;
            case 4:
                // AudioOptions -> MenuObjs -> MuteToggle
                return GetFloatingOptionsMenuObj().transform.GetChild(1).GetChild(1).GetChild(2).gameObject;
            case 5:
                // PositionSliders -> MenuObjs -> POS_XSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(2).GetChild(1).GetChild(0).GetChild(1).gameObject;
            case 6:
                // PositionSliders -> MenuObjs -> POS_YSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(2).GetChild(1).GetChild(1).GetChild(1).gameObject;
            case 7:
                // PositionSliders -> MenuObjs -> POS_ZSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(2).GetChild(1).GetChild(2).GetChild(1).gameObject;
            case 8:
                // RotationSliders -> MenuObjs -> Rotate_XSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(3).GetChild(1).GetChild(0).GetChild(1).gameObject;
            case 9:
                // RotationSliders -> MenuObjs -> Rotate_YSlider -> Slider
                return GetFloatingOptionsMenuObj().transform.GetChild(3).GetChild(1).GetChild(1).GetChild(1).gameObject;
            default:
                return null;
        }
    }

    // Set the CameraRig to video sphere in 360SphereVideo
    private void SetSphereCameraRig()
    {
        Camera cameraRig = null;

        // Detect which camera rig to use based on device model.
        switch (GetDeviceModel())
        {
            case "Oculus Pacific":
                // Set oculus main camera
                cameraRig = GetOculusCameraRig();
                // Disable unused GameObjects/Scripts
#if UNITY_EDITOR
                // Disable the Oculus Go Controller 
                GetOculusGoControllerContainer().SetActive(false);
#else
                // Disable the Oculus Rift Controller 
                GetOculusRiftControllerContainer().SetActive(false);
#endif
                break;

            default:
                // Set default main camera
                cameraRig = GetOculusCameraRig();
                // Disable unused GameObjects/Scripts
#if UNITY_EDITOR
                // Disable the Oculus Go Controller 
                GetOculusGoControllerContainer().SetActive(false);
#endif
                break;
        }
        // Set camera rig to video sphere in 360SphereVideo
        SetSphereVideoObjCameraRig(cameraRig);
    }

    // Get the Headset Controller GameObject
    public GameObject GetControllerGameObject()
    {
        switch (GetDeviceModel())
        {
            case "Oculus Pacific":
#if UNITY_EDITOR
            // Return Oculus Rift Controller GameObject
                return GetOculusRiftControllerContainer();
#else
                // Return Oculus Go Controller GameObject
                // return GetOculusGoControllerContainer(); // disabled it because i was using this to add a menu to the controller and we dont want to do that anymore. This was the fastest way.
                return null;
#endif
                default:
#if UNITY_EDITOR
                // Return Oculus Rift Controller GameObject
                return GetOculusRiftControllerContainer();
#else
                return null;
#endif
        }
    }

    // Return the current CameraRig Container
    public GameObject GetCurrentCameraRigContainer()
    {
        // Detect which camera rig container to return based on device model.
        switch (GetDeviceModel())
        {
            case "Oculus Pacific":
                // Set current main camera container
                return GetOculusContainer();
            default:
                // Set current main camera container
                return GetOculusContainer();
        }
    }

    // -------------------------------------------------------------
    // Handy Code Snippets
    // -------------------------------------------------------------

    // Returns the GameObjects path when used in Debug.Log
    private static string GetGameObjectPath(Transform transform)
    {
        string path = transform.name;
        while (transform.parent != null)
        {
            transform = transform.parent;
            path = transform.name + "/" + path;
        }
        return path;
    }
}
