﻿Shader "Medoptic/VR/InsideSphere Unlit Transparent(stereo+color+fog+alpha) CUSTOM With Fade v3"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "black" {}
		_ChromaTex("Chroma", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)

		[KeywordEnum(None, Top_Bottom, Left_Right, Custom_UV)] Stereo ("Stereo Mode", Float) = 0
		[KeywordEnum(None, Top_Bottom, Left_Right)] AlphaPack("Alpha Pack", Float) = 0
		[Toggle(STEREO_DEBUG)] _StereoDebug ("Stereo Debug Tinting", Float) = 0
		[KeywordEnum(None, EquiRect180)] Layout("Layout", Float) = 0
		[Toggle(HIGH_QUALITY)] _HighQuality ("High Quality", Float) = 0
		[Toggle(APPLY_GAMMA)] _ApplyGamma("Apply Gamma", Float) = 0
		[Toggle(USE_YPCBCR)] _UseYpCbCr("Use YpCbCr", Float) = 0
		[Toggle(USE_OLD_METHOD)] _UseOldFishEye("Use old fisheye", Float) = 0
		_EdgeFeather("Edge Feather", Range (0, 1)) = 0.02
    }
    SubShader
    {
		Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		ZWrite On
		//ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Front
		Lighting Off

        Pass
        {
            CGPROGRAM
			#include "UnityCG.cginc"
			#include "../../AVProVideo/Resources/Shaders/AVProVideo.cginc"
#if HIGH_QUALITY || APPLY_GAMMA
			#pragma target 3.0
#endif
            #pragma vertex vert
            #pragma fragment frag

			#pragma multi_compile_fog
			#pragma multi_compile MONOSCOPIC STEREO_TOP_BOTTOM STEREO_LEFT_RIGHT STEREO_CUSTOM_UV
			#pragma multi_compile ALPHAPACK_NONE ALPHAPACK_TOP_BOTTOM ALPHAPACK_LEFT_RIGHT

			// TODO: Change XX_OFF to __ for Unity 5.0 and above
			// this was just added for Unity 4.x compatibility as __ causes
			// Android and iOS builds to fail the shader
			#pragma multi_compile STEREO_DEBUG_OFF STEREO_DEBUG
			#pragma multi_compile HIGH_QUALITY_OFF HIGH_QUALITY
			#pragma multi_compile APPLY_GAMMA_OFF APPLY_GAMMA
			#pragma multi_compile USE_YPCBCR_OFF USE_YPCBCR
			#pragma multi_compile LAYOUT_NONE LAYOUT_EQUIRECT180
			#pragma multi_compile USE_OLD_METHOD_OFF USE_OLD_METHOD 

            struct appdata
            {
                float4 vertex : POSITION; // vertex position
#if HIGH_QUALITY
				float3 normal : NORMAL;
#else
                float2 uv : TEXCOORD0; // texture coordinate			
	#if STEREO_CUSTOM_UV
				float2 uv2 : TEXCOORD1;	// Custom uv set for right eye (left eye is in TEXCOORD0)
	#endif
#endif
				
            };

            struct v2f
            {
                float4 vertex : SV_POSITION; // clip space position
#if HIGH_QUALITY
				float3 normal : TEXCOORD0;
				
	#if STEREO_TOP_BOTTOM || STEREO_LEFT_RIGHT
				float4 scaleOffset : TEXCOORD1; // texture coordinate
		#if UNITY_VERSION >= 500
				UNITY_FOG_COORDS(2)
		#endif
	#else
		#if UNITY_VERSION >= 500
				UNITY_FOG_COORDS(1)
		#endif
	#endif
#else
                float4 uv : TEXCOORD0; // texture coordinate
	#if UNITY_VERSION >= 500
				UNITY_FOG_COORDS(1)
	#endif
#endif

#if STEREO_DEBUG
				float4 tint : COLOR;
#endif
            };

            uniform sampler2D _MainTex;
#if USE_YPCBCR
			uniform sampler2D _ChromaTex;
#endif
			uniform float4 _MainTex_ST;
			uniform float4 _MainTex_TexelSize;
			uniform float3 _cameraPosition;
			uniform fixed4 _Color;
			uniform float _EdgeFeather;

            v2f vert (appdata v)
            {
                v2f o;
				o.vertex = XFormObjectToClip(v.vertex);
#if !HIGH_QUALITY
				o.uv.zw = 0.0;
				o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				#if LAYOUT_EQUIRECT180
				o.uv.x = ((o.uv.x - 0.5) * 2.0) + 0.5;
				// Set value for clipping if UV area is behind viewer
				o.uv.z = -1.0;
				if (v.uv.x > 0.25 && v.uv.x < 0.75)
				{
					o.uv.z = 1.0;
				}
				#endif
                o.uv.xy = float2(1.0-o.uv.x, o.uv.y);
#endif

#if STEREO_TOP_BOTTOM || STEREO_LEFT_RIGHT
				float4 scaleOffset = GetStereoScaleOffset(IsStereoEyeLeft(_cameraPosition, UNITY_MATRIX_V[0].xyz), _MainTex_ST.y < 0.0);

				#if !HIGH_QUALITY
				o.uv.xy *= scaleOffset.xy;
				o.uv.xy += scaleOffset.zw;
				#else
				o.scaleOffset = scaleOffset;
				#endif
#elif STEREO_CUSTOM_UV && !HIGH_QUALITY
				if (!IsStereoEyeLeft(_cameraPosition, UNITY_MATRIX_V[0].xyz))
				{
					o.uv.xy = TRANSFORM_TEX(v.uv2, _MainTex);
					o.uv.xy = float2(1.0 - o.uv.x, o.uv.y);
				}
#endif
				
#if !HIGH_QUALITY
	#if ALPHAPACK_TOP_BOTTOM || ALPHAPACK_LEFT_RIGHT
				o.uv = OffsetAlphaPackingUV(_MainTex_TexelSize.xy, o.uv.xy, _MainTex_ST.y > 0.0);
	#endif
#endif

#if HIGH_QUALITY
				o.normal = v.normal;
#endif

				#if STEREO_DEBUG
				o.tint = GetStereoDebugTint(IsStereoEyeLeft(_cameraPosition, UNITY_MATRIX_V[0].xyz));
				#endif

#if UNITY_VERSION >= 500
				UNITY_TRANSFER_FOG(o, o.vertex);
#endif
                return o;
			}

			//Returns the UVs to read from a texture and not the color like the original version
			float2 fish2sphere( float2 uv, float2 center ) { 
    				float2 iResolution = float2(1024.0, 768.0);
    				float4 pfish;
    				float theta,phi,r;
    				float3 psph;
	
    				float PI = 3.14159265; 
    				float FOV = 3.141592654; // FOV of the fisheye, eg: 180 degrees 
    
				// Polar angles
    				theta = 2.0 * PI * (uv.x - center.x); // -pi to pi
				phi = PI * (uv.y - 0.5);	// -pi/2 to pi/2

				// Vector in 3D space
				psph.x = cos(phi) * sin(theta);
				psph.y = cos(phi) * cos(theta);
				psph.z = sin(phi);
	
				// Calculate fisheye angle and radius
				theta = atan2( psph.z, psph.x);
				phi = atan2(sqrt(psph.x*psph.x+psph.z*psph.z), psph.y); 
    
    				r = phi / FOV; 

    				// Pixel in fisheye space
    				pfish.x = center.x +  (r / 2.0) * cos(theta);
    				pfish.y = 0.5 + r * sin(theta);

    				//float4 col = tex2D(_MainTex, float2(pfish.x, pfish.y));
                                        
				return float2(pfish.x, pfish.y); 
			}

			float2 fish2eye( float2 uv ){
			 	float2 pfish;
				float3 psph;
				float4 missingcolour = float4(0,0.0,0.0,1.0);
				float longitude,latitude;
				float theta,phi,r;
				float PI = 3.1415926536;

				// Fisheye pair image size
				float WIDTH = 4096.;
				float HEIGHT = 2048.;

				// Lens parameters
				float FOV = 220.0; // Degrees
				float CENTERX;
				float CENTERY;
				float RADIUS;
				float LENSA1 = 0.6622;
				float LENSA2 = -0.0163;
				float LENSA3 = 0.0029;
				float LENSA4 = -0.0169;

				FOV = 0.5 * FOV * PI / 180.0; // Half FOV in radians

				// Treat left and right halves different
				// Compute longitude and latitude
				if (uv.x < 0.5) {
					CENTERX = 1045.0; // Left side of pair sample image
					CENTERY = 1030.0;
					RADIUS = 1110.0;
					longitude = 2.0 * PI * (uv.x - 0.25); // -pi ... pi
				} else {
					CENTERX = 3096.0; // Right side of pair sample image
					CENTERY = 1030.0;
					RADIUS = 1110.0;
					longitude = 2.0 * PI * (uv.x - 0.75); // -pi ... pi
				}
				latitude = PI * (uv.y - 0.5); // -pi/2 ... pi/2

				// Vector into 3D world
				psph.x = cos(latitude) * sin(longitude);
				psph.y = cos(latitude) * cos(longitude);
				psph.z = sin(latitude);

				// Angles in fisheye space
				theta = atan2(psph.z,psph.x); // -pi ... pi
				phi = atan2(sqrt(psph.x*psph.x+psph.z*psph.z),psph.y); // 0 ... (fisheye fov)/2

				// radius from phi, no lens correction
				//r =  phi / FOV; // 0 ... 1

				// radius from phi, lens correction
				r = phi * (LENSA1 + phi * (LENSA2 + phi * (LENSA3 + phi * LENSA4))); // 0 ... 0.5

				// Pixel in fisheye space
				r *= (RADIUS/WIDTH);
				pfish.x = (CENTERX/WIDTH) + r * cos(theta);
				pfish.y = ( ((HEIGHT-1.0-CENTERY)/HEIGHT) + (WIDTH/HEIGHT) * r * sin(theta) );

				// Clip to fisheye circle


				if (r > 1.0 || pfish.x < 0.0 || pfish.y < 0.0 || pfish.x > 1.0 || pfish.y > 1.0) {
					//return float2(-1.0, -1.0);
				} else {
					
				}
				return ( pfish );
			}			

            
            fixed4 frag (v2f i) : SV_Target
			{
				float4 uv = 0;

								
#if HIGH_QUALITY
				float3 n = normalize(i.normal);
				#if LAYOUT_EQUIRECT180
				clip(-n.z);	// Clip pixels on the back of the sphere
				#endif

				float M_1_PI = 1.0 / 3.1415926535897932384626433832795;	
				float M_1_2PI = 1.0 / 6.283185307179586476925286766559;
				uv.x = 0.5 - atan2(n.z, n.x) * M_1_2PI;
				uv.y = 0.5 - asin(-n.y) * M_1_PI;
				uv.x += 0.75;
				uv.x = fmod(uv.x, 1.0);
				//uv.x = uv.x % 1.0;
				uv.xy = TRANSFORM_TEX(uv, _MainTex);
				#if LAYOUT_EQUIRECT180
				uv.x = ((uv.x - 0.5) * 2.0) + 0.5;
				#endif

				#if STEREO_TOP_BOTTOM | STEREO_LEFT_RIGHT
				uv.xy *= i.scaleOffset.xy;
				uv.xy += i.scaleOffset.zw;
				#endif

				#if ALPHAPACK_TOP_BOTTOM | ALPHAPACK_LEFT_RIGHT
				uv = OffsetAlphaPackingUV(_MainTex_TexelSize.xy, uv.xy, _MainTex_ST.y < 0.0);
				#endif
#else


				uv = i.uv;
				#if LAYOUT_EQUIRECT180
				clip(i.uv.z);	// Clip pixels on the back of the sphere
				#endif


#endif

				float2 uv2d=float2(uv.x, uv.y);
#if USE_OLD_METHOD
				if(uv2d.x < 0.5) {
    				//This is the left half of the screen
    				uv.xy = fish2sphere(uv2d, float2(0.25, 0.5));
				} else {
    				//Right half of the screen
    				uv.xy = fish2sphere(uv2d, float2(0.75, 0.5));
				}
				#else
					uv.xy = fish2eye(uv2d);
					//clip(uv.xy);
				#endif


#if USE_YPCBCR
	#if SHADER_API_METAL || SHADER_API_GLES || SHADER_API_GLES3
				float3 ypcbcr = float3(tex2D(_MainTex, uv).r, tex2D(_ChromaTex, uv).rg);
	#else
				float3 ypcbcr = float3(tex2D(_MainTex, uv).r, tex2D(_ChromaTex, uv).ra);
	#endif
				fixed4 col = fixed4(Convert420YpCbCr8ToRGB(ypcbcr), 1.0);
#else
                fixed4 col = tex2D(_MainTex, uv);
#endif

#if APPLY_GAMMA
				col.rgb = GammaToLinear(col.rgb);
#endif

#if ALPHAPACK_TOP_BOTTOM || ALPHAPACK_LEFT_RIGHT
				// Sample the alpha
				fixed4 alpha = tex2D(_MainTex, uv.zw);
#if APPLY_GAMMA
				alpha.rgb = GammaToLinear(alpha.rgb);
#endif

				col.a = (alpha.r + alpha.g + alpha.b) / 3.0;
				//col.a = (alpha.r + alpha.g + alpha.g + alpha.b) / 4.0;

				//clip(col.a - 0.01);
#endif

#if STEREO_DEBUG
				col *= i.tint;
#endif

				col *= _Color;

#if UNITY_VERSION >= 500
				UNITY_APPLY_FOG(i.fogCoord, col);
#endif

#if LAYOUT_EQUIRECT180
				// Apply edge feathering based on UV mapping - this is useful if you're using a hemisphere mesh for 180 degree video and want to have soft edges
				if (_EdgeFeather > 0.0)
				{
					float4 featherDirection = float4(0.0, 0.0, 1.0, 1.0);
					
#if STEREO_TOP_BOTTOM 
					if (uv2d.y > 0.5)
					{
						featherDirection.y = 0.5;
					}
					else
					{
						featherDirection.w = 0.5;
					}
#endif

#if STEREO_LEFT_RIGHT
					if (uv2d.x > 0.5)
					{
						featherDirection.x = 0.5;
					}
					else
					{
						featherDirection.z = 0.5;
					}
#endif


#if ALPHAPACK_TOP_BOTTOM
					featherDirection.w *= 0.5;
#endif

#if ALPHAPACK_LEFT_RIGHT
					featherDirection.z *= 0.5;
#endif

					float d = min(uv2d.x - featherDirection.x, min((uv2d.y - featherDirection.y), min(featherDirection.z - uv2d.x, featherDirection.w - uv2d.y)));
					float a = smoothstep(0.0, _EdgeFeather, d);
					col.a *= a;
				}
#endif

				return col;

			}
            ENDCG
        }
    }
}